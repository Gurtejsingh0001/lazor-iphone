//
//  LazorNewsTblCell.swift
//  Lazorapp
//
//  Created by SPDY on 28/05/21.
//

import UIKit

class LazorNewsTblCell: UITableViewCell {

    @IBOutlet weak var imgViewNews: UIImageView!
    @IBOutlet weak var lblTitleNews: UILabel!
    @IBOutlet weak var lblDescNews: UILabel!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblDateNews: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        let str = "Firstly identify and define the firstly identify and define the environment where this will environment"
//        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Navigo-ExtraLight", size: 11.0)!, NSAttributedString.Key.foregroundColor : UIColor.black]
//        
//          let attrs2 = [NSAttributedString.Key.font : UIFont(name: "Navigo", size: 13.0)!, NSAttributedString.Key.foregroundColor : colorGreen]
//
//        let attributedString1 = NSMutableAttributedString(string: str, attributes:attrs1)
//          let attributedString2 = NSMutableAttributedString(string:" Read More", attributes:attrs2)
//
//
//        attributedString1.append(attributedString2)
//          lblDescNews.attributedText = attributedString1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


class LazorNewsDetailTblCell: UITableViewCell {

    @IBOutlet weak var lblDescNews: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
