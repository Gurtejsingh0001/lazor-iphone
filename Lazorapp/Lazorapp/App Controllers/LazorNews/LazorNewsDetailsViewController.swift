//
//  LazorNewsDetailsViewController.swift
//  Lazorapp
//
//  Created by SPDY on 28/05/21.
//

import UIKit
import SVProgressHUD

class LazorNewsDetailsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var tblViewNewsDetails: UITableView!
    
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    var strNewsDesc = String()
    
    
    var arrNewsList = [LazorNewsData]()
    var newsId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getLazorNewsDetailApi()
    }
    

    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblViewNewsDetails.dequeueReusableCell(withIdentifier: "LazorNewsDetailTblCell") as! LazorNewsDetailTblCell
        cell.lblDescNews.text = strNewsDesc
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    //MARK: - API Methods
    func getLazorNewsDetailApi()  {
        SVProgressHUD.show()
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_LazorNews! as String + "/" + newsId as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    if let dictTemp = result.value(forKey: "data") as? NSDictionary{
                        self.lblTitle.text = dictTemp["title"] as? String
                        self.lblDate.text = dictTemp["created_at"] as? String
                        let category = dictTemp["category"] as? NSDictionary
                        self.lblCategory.text = category?["name"] as? String
                        self.strNewsDesc = dictTemp["desc"] as? String ?? ""
                        let imgURL = URL(string: dictTemp["imgae"] as? String ?? "")
                        self.imgNews.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Lazor"))
                    }
                   
                    self.tblViewNewsDetails.reloadData()
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }

}
