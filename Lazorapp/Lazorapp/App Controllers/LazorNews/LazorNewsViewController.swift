//
//  LazorNewsViewController.swift
//  Lazorapp
//
//  Created by SPDY on 28/05/21.
//

import UIKit
import SVProgressHUD

class LazorNewsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var tblViewLazorNews: UITableView!
    var arrNewsList = [LazorNewsData]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getLazorNewsApi()
    }
    

    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNewsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblViewLazorNews.dequeueReusableCell(withIdentifier: "LazorNewsTblCell") as! LazorNewsTblCell
        let data = self.arrNewsList[indexPath.row]
        cell.lblTitleNews.text = data.title
        cell.lblCategoryName.text = data.category[0].name
        cell.lblDateNews.text = data.created_at
        let imgURL = URL(string: data.imgae)
        cell.imgViewNews.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Lazor"))
        
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "Navigo-ExtraLight", size: 11.0)!, NSAttributedString.Key.foregroundColor : UIColor.black]
        let attrs2 = [NSAttributedString.Key.font : UIFont(name: "Navigo", size: 13.0)!, NSAttributedString.Key.foregroundColor : colorGreen]

        let attributedString1 = NSMutableAttributedString(string: data.desc, attributes:attrs1)
     //   let attributedString2 = NSMutableAttributedString(string:" Read More", attributes:attrs2)
     //   attributedString1.append(attributedString2)
        cell.lblDescNews.attributedText = attributedString1
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.arrNewsList[indexPath.row]
        let vc = storyboard?.instantiateViewController(withIdentifier: "LazorNewsDetailsViewController") as! LazorNewsDetailsViewController
        vc.newsId = data.id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK: - API Methods
    func getLazorNewsApi()  {
        SVProgressHUD.show()
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_LazorNews! as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    if let arrTemp = result.value(forKey: "data") as? NSArray{
                        self.arrNewsList.removeAll()
                        for dict in arrTemp {
                            let data = LazorNewsData.init(data: dict as! [String : Any])
                            self.arrNewsList.append(data)
                        }
                        self.tblViewLazorNews.reloadData()
                    }
                   
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }

    
    
}
