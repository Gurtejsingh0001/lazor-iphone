//
//  BankingInfoViewController.swift
//  Lazorapp
//
//  Created by SPDY on 26/05/21.
//

import UIKit
import SVProgressHUD

class BankingInfoViewController: BaseViewController {

    @IBOutlet weak var viewBankName: UIView!
    @IBOutlet weak var viewRoutingNum: UIView!
    @IBOutlet weak var viewAccountNum: UIView!
    
    
    @IBOutlet weak var txtBankName: UITextField!
    @IBOutlet weak var txtRoutingNum: UITextField!
    @IBOutlet weak var txtAccountNum: UITextField!
    
    
    @IBOutlet weak var btnCheck1TC: UIButton!
    @IBOutlet weak var btnCheck2PP: UIButton!
    
    var btnCheck1Bool: Bool! = false
    var btnCheck2Bool: Bool! = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewBankName.addGrayViewBorder()
        viewRoutingNum.addGrayViewBorder()
        viewAccountNum.addGrayViewBorder()
       
    }
    

    //MARK:- UIButtons Action
    
    @IBAction func btnCheckBoxTCAction(_ sender: UIButton) {
        if sender.tag == 0{
            btnCheck1TC.setImage(UIImage.init(named: "check_ic"), for: .normal)
            sender.tag = 1
            btnCheck1Bool = true
        }else{
            btnCheck1TC.setImage(UIImage.init(named: "Uncheck_ic"), for: .normal)
            sender.tag = 0
            btnCheck1Bool = false
        }
    }
    
    @IBAction func btnCheckBoxPPAction(_ sender: UIButton) {
        if sender.tag == 0{
            btnCheck2PP.setImage(UIImage.init(named: "check_ic"), for: .normal)
            sender.tag = 1
            btnCheck2Bool = true
        }else{
            btnCheck2PP.setImage(UIImage.init(named: "Uncheck_ic"), for: .normal)
            sender.tag = 0
            btnCheck2Bool = false
        }
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
      
        if txtBankName.text?.isEmpty == true{
            self.showErrorMessage(msgBankName);
        }else if txtRoutingNum.text?.isEmpty == true{
            self.showErrorMessage(msgRoutingNumber);
        }else if txtAccountNum.text?.isEmpty == true{
            self.showErrorMessage(msgAccountNumber);
        }else if btnCheck1Bool == false{
            self.showErrorMessage(msgCheckBoxTermsConditions);
        }else if btnCheck2Bool == false{
            self.showErrorMessage(msgCheckBoxPrivacyPolicy);
        }else{
            bankDetailsApi()
        }
        
        
    }
    
    //MARK: - API Methods
    func bankDetailsApi()  {
        SVProgressHUD.show()
        
        let dictParam : NSDictionary? =  ["bank_name": txtBankName.text!, "routing_number": txtRoutingNum.text!, "account_number": txtAccountNum.text!];
        
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_BankDetails! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddDebitCardViewController") as! AddDebitCardViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }

    
    
}
