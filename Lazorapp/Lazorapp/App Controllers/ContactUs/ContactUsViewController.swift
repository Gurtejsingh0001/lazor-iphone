//
//  ContactUsViewController.swift
//  Lazorapp
//
//  Created by SPDY on 28/05/21.
//

import UIKit

class ContactUsViewController: BaseViewController,UITextViewDelegate {

    
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewCompanyName: UIView!
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewReason: UIView!
    @IBOutlet weak var viewMessage: UIView!
    
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtCompanyName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtReson: UITextField!
    @IBOutlet weak var txtViewMessage: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewName.addGrayViewBorder()
        viewCompanyName.addGrayViewBorder()
        viewPhone.addGrayViewBorder()
        viewEmail.addGrayViewBorder()
        viewReason.addGrayViewBorder()
        viewMessage.addGrayViewBorder()

    }
    
    //MARK:- UITextView Delegets
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Message"{
            self.txtViewMessage.text = ""
            self.txtViewMessage.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            self.txtViewMessage.text = "Message"
            self.txtViewMessage.textColor = colorDarkGray
        }
    }
    
    
    //MARK:- UIButtons Action
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        
    }
    
}
