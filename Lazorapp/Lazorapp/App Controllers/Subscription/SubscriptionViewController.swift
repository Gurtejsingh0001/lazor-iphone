//
//  SubscriptionViewController.swift
//  Lazorapp
//
//  Created by SPDY on 04/03/1943 Saka.
//

import UIKit

class SubscriptionViewController: BaseViewController {

    
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var btn1Doner: UIButton!
    @IBOutlet weak var btn2AcceptSub: UIButton!
    @IBOutlet weak var btn3PayLater: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btn1Doner.addGreenBorderButton()
        btn2AcceptSub.addGreenBorderButton()
        btn3PayLater.addGreenBorderButton()
        
        viewMain.layer.borderWidth = 1
        viewMain.layer.borderColor = colorDarkGray.cgColor
        
    }
    
    
    //MARK:- UIButtons Action
    
    @IBAction func btnYearlySubscriptionAction(_ sender: Any) {
        
//        let strMain = UIStoryboard(name: "Main", bundle: nil)
//        let vc = strMain.instantiateViewController(withIdentifier: "DashboardNavigation")
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.window?.rootViewController = vc
        
    }
    

}

