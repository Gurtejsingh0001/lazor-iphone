//
//  QuestionsTableViewCell.swift
//  Lazorapp
//
//  Created by SPDY on 04/03/1943 Saka.
//

import UIKit

class QuestionsTableViewCell: UITableViewCell,UITextViewDelegate {

    @IBOutlet weak var viewMainQuestion: UIView!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var btnDown: UIButton!
    
    
    @IBOutlet weak var viewMainAnswer: UIView!
    @IBOutlet weak var txtViewAnswer: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewMainQuestion.addGrayViewBorder()
        viewMainAnswer.addGrayViewBorder()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- UITextView Delegets
    func textViewDidBeginEditing(_ textView: UITextView) {
//        if textView.text == "Answer"{
//            self.txtViewAnswer.text = ""
//            self.txtViewAnswer.textColor = UIColor.black
//        }
        
    }

    func textViewDidEndEditing(_ textView: UITextView) {
//        if textView.text == ""{
//            self.txtViewAnswer.text = "Answer"
//            self.txtViewAnswer.textColor = colorDarkGray
//        }
        
        
    }
    

}


