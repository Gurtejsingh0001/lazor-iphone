//
//  QuestionViewController.swift
//  Lazorapp
//
//  Created by SPDY on 04/03/1943 Saka.
//

import UIKit
import SVProgressHUD
import Foundation
import CoreData
import Alamofire

class QueAnsData: NSObject {
    
    @objc var question_id = ""
    @objc var answer = ""

    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
             self.setValue(value, forKey: key)
        }
    }
    
    override init() {
        super.init()
    }
    
}

class QuestionViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {

    
    @IBOutlet weak var tblQuestionsList: UITableView!
    var arrQuestionList = [SequrityQuestions]()
    var dictQuestionAns = [QueAnsData]()
    var txtView : UITextView!
    var dictTemp = [String : String]()
    var indexNum = Int()
 
    typealias ASCompletionBlock = (_ result: NSDictionary, _ error: Error?, _ success: Bool) -> Void
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getQuestionApi()
    }


    //MARK:- UITextView Delegets
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Answer"{
            textView.text = ""
            textView.textColor = UIColor.black
        }

    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            textView.text = "Answer"
            textView.textColor = colorDarkGray
        }

        let data = arrQuestionList[textView.tag]
        data.answer = textView.text

    }
    
    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQuestionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblQuestionsList.dequeueReusableCell(withIdentifier: "QuestionsTableViewCell") as! QuestionsTableViewCell
        let data = arrQuestionList[indexPath.row]
        cell.lblQuestion.text = data.question
        
        cell.txtViewAnswer.text = data.answer
        
        cell.txtViewAnswer.tag = indexPath.row
        self.txtView = cell.txtViewAnswer
        self.txtView.delegate = self
        
        if data.answer == "Answer"{
            cell.txtViewAnswer.textColor = colorDarkGray
        }else{
            cell.txtViewAnswer.textColor = UIColor.black
        }
     //   cell.txtViewAnswer.text = data.answer
     //   cell.txtViewAnswer.delegate = self
        
    
        
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    

    //MARK:- UIButtons Action
    @IBAction func btnContinueAction(_ sender: Any) {
        
        print(arrQuestionList)
        dictQuestionAns.removeAll()
        for i in arrQuestionList{
            if i.answer != "" && i.answer != "Answer"{
                dictTemp.updateValue(i.id, forKey: "question_id")
                dictTemp.updateValue(i.answer, forKey: "answer")
                let data = QueAnsData.init(data: dictTemp)
                dictQuestionAns.append(data)
                print(dictQuestionAns)
                dictTemp.removeAll()
            }else{
                print(i.id)
            }
        }
        
        submitQuestionsApi(arrParam: dictQuestionAns)
       
        
    }
    
    
    //MARK: - API Methods
    func getQuestionApi()  {
        SVProgressHUD.show()
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_GetQuestions! as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    
                    if let arrTemp = result.value(forKey: "data") as? NSArray{
                        self.arrQuestionList.removeAll()
                        for dict in arrTemp {
                            let data = SequrityQuestions.init(data: dict as! [String : Any])
                            self.arrQuestionList.append(data)
                        }
                        self.tblQuestionsList.reloadData()
                    }
                
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    
    func submitQuestionsApi(arrParam: [QueAnsData])  {
        
        
        var tempDict = [String: Any]()//["question_id":"","answer":""]
        var arrTempDict = [tempDict]
        arrTempDict.removeAll()
        tempDict.removeAll()
        for i in arrParam{
            tempDict.updateValue(i.question_id, forKey: "question_id")
            tempDict.updateValue(i.answer, forKey: "answer")
            arrTempDict.append(tempDict)
        }
        print(tempDict)
        print(arrTempDict)
        
        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["security_questions": arrTempDict];
        print(dictParam as Any)
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_SubmitQuestions! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "BankingInfoViewController") as! BankingInfoViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }

    
}
