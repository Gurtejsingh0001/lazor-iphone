//
//  ProfileCollecCell.swift
//  Lazorapp
//
//  Created by SPDY on 16/06/21.
//

import UIKit

class ProfileCollecCell: UICollectionViewCell {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblCategoryName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewMain.addGrayViewBorder()
    }
    
}


class ProfileVideosCollecCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnVideo: UIButton!
    
}


class ProfileHeaderCollecCell: UICollectionReusableView {
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserAbout: UILabel!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var btnShareProfile: UIButton!
    @IBOutlet weak var btnBronze: UIButton!
    
    @IBOutlet weak var imgBadge: UIImageView!
    
    @IBOutlet weak var imgGreenBG1: UIImageView!
    @IBOutlet weak var imgGreenBG2: UIImageView!
    @IBOutlet weak var imgGreenBG3: UIImageView!
    
    @IBOutlet weak var btnDonationTo: UIButton!
    @IBOutlet weak var btnDonationFrom: UIButton!
    @IBOutlet weak var btnVideos: UIButton!
    
    
}
