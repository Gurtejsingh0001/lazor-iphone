//
//  ProfileNewViewController.swift
//  Lazorapp
//
//  Created by SPDY on 16/06/21.
//

import UIKit
import SVProgressHUD

class ProfileNewViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    
    @IBOutlet weak var collecViewList: UICollectionView!
    @IBOutlet weak var btnEdit: UIButton!
    
    var btnIndex = Int()
    var strUserId = String()
    
    var strName = String()
    var strAbout = String()
    var strImage = String()
    var strLocation = String()
    var strBadge = String()
    
    
    var arrDonationList = [DonationBaseData]()
    var arrDonersList = [DonationBaseData]()
    var arrVideoList = [DashboardVideosData]()
    var strController = ""

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if strController == "userProfile"{
            btnEdit.isHidden = false
        }else{
            btnEdit.isHidden = true
        }
        
        getProfileApi()
    }
    
    
    //MARK:- UICollectionView Delegats
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if btnIndex == 1 {
            if arrDonersList.count != 0{
                return arrDonersList.count
            }
        }else if btnIndex == 2{
            if arrVideoList.count != 0{
                return arrVideoList.count
            }
        }else{
            if arrDonationList.count != 0{
                return arrDonationList.count
            }
        }
         return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ProfileHeaderCollecCell", for: indexPath) as! ProfileHeaderCollecCell
            
            view.lblUserName.text = strName
            view.lblUserAbout.text = strAbout
            view.btnLocation.setTitle(strLocation, for: .normal)
            
            if strBadge == ""{
                view.btnBronze.setTitle("No Badge", for: .normal)
            }else{
                view.btnBronze.setTitle(strBadge, for: .normal)
                switch strBadge {
                case "Bronze":
                    view.imgBadge.setImage(UIImage.init(named: "Group 3851")!)
                case "Silver":
                    view.imgBadge.setImage(UIImage.init(named: "Group 4351")!)
                case "Gold":
                    view.imgBadge.setImage(UIImage.init(named: "Group 4104")!)
                case "Platinum":
                    view.imgBadge.setImage(UIImage.init(named: "Group 4225")!)
                default:
                    view.imgBadge.setImage(UIImage.init(named: "Group 4351")!)
                }
            }
            
        
            let imgURL = URL(string: strImage)
            if #available(iOS 13.0, *) {
                view.imgUserProfile.sd_setImage(with: imgURL, placeholderImage: UIImage(systemName: "person.fill"))
            } else {
                view.imgUserProfile.sd_setImage(with: imgURL, placeholderImage:  UIImage(named: "Mask Group 84"))
            }
            
            switch btnIndex {
                case 0:
                    btnIndex = 0
                    view.imgGreenBG1.isHidden = false
                    view.imgGreenBG2.isHidden = true
                    view.imgGreenBG3.isHidden = true
                    
                    view.btnDonationTo.setTitleColor(UIColor.white, for: .normal)
                    view.btnDonationFrom.setTitleColor(UIColor.black, for: .normal)
                    view.btnVideos.setTitleColor(UIColor.black, for: .normal)
                case 1:
                    btnIndex = 1
                    view.imgGreenBG1.isHidden = true
                    view.imgGreenBG2.isHidden = false
                    view.imgGreenBG3.isHidden = true
                    
                    view.btnDonationTo.setTitleColor(UIColor.black, for: .normal)
                    view.btnDonationFrom.setTitleColor(UIColor.white, for: .normal)
                    view.btnVideos.setTitleColor(UIColor.black, for: .normal)
                case 2:
                    btnIndex = 2
                    view.imgGreenBG1.isHidden = true
                    view.imgGreenBG2.isHidden = true
                    view.imgGreenBG3.isHidden = false
                    
                    view.btnDonationTo.setTitleColor(UIColor.black, for: .normal)
                    view.btnDonationFrom.setTitleColor(UIColor.black, for: .normal)
                    view.btnVideos.setTitleColor(UIColor.white, for: .normal)
                default:
                    print("No Tag")
            }
            return view
        }
        fatalError("Unexpected kind")
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if btnIndex == 2 {
            if self.arrVideoList.count != 0{
                let cell = self.collecViewList.dequeueReusableCell(withReuseIdentifier: "ProfileVideosCollecCell", for: indexPath) as! ProfileVideosCollecCell
                let data = self.arrVideoList[indexPath.row]
                let imgURL = URL(string: data.thumbnail)
                cell.imgView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Lazor"))
                return cell
            }
           
        }else{
            
            if btnIndex == 1{
                if self.arrDonersList.count != 0{
                    let cell = self.collecViewList.dequeueReusableCell(withReuseIdentifier: "ProfileCollecCell", for: indexPath) as! ProfileCollecCell
                    let data = self.arrDonersList[indexPath.row]
                    cell.lblAmount.text = data.donation_request[0].donation_amount
                    cell.lblCategoryName.text = data.donation_request[0].category[0].name
                    cell.lblTitle.text = data.donation[0].name
                    let imgURL = URL(string: data.donation[0].image)
                    cell.imgView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Lazor"))
                return cell
                }
            }else{
                if self.arrDonationList.count != 0{
                    let cell = self.collecViewList.dequeueReusableCell(withReuseIdentifier: "ProfileCollecCell", for: indexPath) as! ProfileCollecCell
                    let data = self.arrDonationList[indexPath.row]
                    cell.lblAmount.text = data.donation_request[0].donation_amount
                    cell.lblCategoryName.text = data.donation_request[0].category[0].name
                    cell.lblTitle.text = data.donation_request[0].caption
                    let imgURL = URL(string: data.donation_request[0].user[0].image)
                    cell.imgView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Mask Group 84"))
                    return cell
                }
            }
            
           
        }
        let cell = self.collecViewList.dequeueReusableCell(withReuseIdentifier: "NoDataCollecCell", for: indexPath)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if btnIndex == 2{
            if self.arrVideoList.count != 0{
                let data = self.arrVideoList[indexPath.row]
                let vc = storyboard?.instantiateViewController(withIdentifier: "VideoViewController") as! VideoViewController
                vc.videoId = data.id
                self.navigationController?.pushViewController(vc, animated: true)
            }
         
        }else if btnIndex == 1{
            if self.arrDonersList.count != 0{
                let data = self.arrDonersList[indexPath.row]
                let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileNewViewController") as! ProfileNewViewController
                vc.strUserId = data.donation[0].id
                self.navigationController?.pushViewController(vc, animated: true)
            }
           
        }else{
            if self.arrDonationList.count != 0{
                let data = self.arrDonationList[indexPath.row]
                let vc = storyboard?.instantiateViewController(withIdentifier: "ReelAllDonerViewController") as! ReelAllDonerViewController
                vc.userId = data.donation_request[0].id
                self.navigationController?.pushViewController(vc, animated: true)
            }
          
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if btnIndex == 2{
            if self.arrVideoList.count != 0{
                return CGSize(width: (view.frame.width - 55) / 3, height: 150)
            }
            
        }else{
            if self.arrDonersList.count != 0 {
                return CGSize(width: view.frame.width, height: 100)
            }else if self.arrDonationList.count != 0{
                return CGSize(width: view.frame.width, height: 100)
            }
            
        }
        return CGSize(width: view.frame.width, height: 200)
    }
    

    //MARK:- UIButtons Action
    
    @IBAction func btnEditAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btnShareProfileAction(_ sender: Any) {
        
        // Setting description
//         let firstActivityItem = "Lazor App.."
//
//         // Setting url
//         let secondActivityItem : NSURL = NSURL(string: "LazorApp.com/")!
//
//         // If you want to use an image
//         let image : UIImage = UIImage(named: "Group 3645")!
//         let activityViewController : UIActivityViewController = UIActivityViewController(
//             activityItems: [firstActivityItem, secondActivityItem, image], applicationActivities: nil)
        
        let someText:String = "com.Lazor://main"
        let objectsToShare:URL = URL(string: "com.Lazor://main")!
        let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
        let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
         
         // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = ((sender ) as! UIView)
         
         // This line remove the arrow of the popover to show in iPad
         activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
         activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
         
         // Pre-configuring activity items
        if #available(iOS 13.0, *) {
            activityViewController.activityItemsConfiguration = [
                UIActivity.ActivityType.message
            ] as? UIActivityItemsConfigurationReading
        } else {
            // Fallback on earlier versions
        }
         
         // Anything you want to exclude
         activityViewController.excludedActivityTypes = [
             UIActivity.ActivityType.postToWeibo,
             UIActivity.ActivityType.print,
             UIActivity.ActivityType.assignToContact,
             UIActivity.ActivityType.saveToCameraRoll,
             UIActivity.ActivityType.addToReadingList,
             UIActivity.ActivityType.postToFlickr,
             UIActivity.ActivityType.postToVimeo,
             UIActivity.ActivityType.postToTencentWeibo,
             UIActivity.ActivityType.postToFacebook
         ]
         
        if #available(iOS 13.0, *) {
            activityViewController.isModalInPresentation = true
        } else {
            // Fallback on earlier versions
        }
         self.present(activityViewController, animated: true, completion: nil)
    
        
    }
    
    @IBAction func btnBronzeAction(_ sender: Any) {
        
        
    }
    
    @IBAction func btnDonationToAction(_ sender: UIButton) {
       
        self.btnIndex = sender.tag
        self.collecViewList.reloadData()
        
//        let vc = storyboard?.instantiateViewController(withIdentifier: "ReelAllDonerViewController") as! ReelAllDonerViewController
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK:- Api's Method
    func getProfileApi()  {
        SVProgressHUD.show()
        print(strUserId)
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_Profile! as String + "/" + strUserId)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    if let dictTemp = result.value(forKey: "data") as? NSDictionary{
                        
                        self.strName = dictTemp["name"] as? String ?? ""
                        self.strAbout = dictTemp["about"] as? String ?? ""
                        self.strLocation = dictTemp["address"] as? String ?? ""
                        self.strImage = dictTemp["image"] as? String ?? ""
                        self.strBadge = dictTemp["badge"] as? String ?? ""
                        self.collecViewList.reloadData()
                    }
                
                    self.getDonationListApi()
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }

    
    func getDonationListApi()  {
        SVProgressHUD.show()
        
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_MyDonation! as String + strUserId + "&page=1" as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    if let arrTemp = result.value(forKey: "data") as? NSArray{
                        self.arrDonationList.removeAll()
                        for dict in arrTemp {
                            let data = DonationBaseData.init(data: dict as! [String : Any])
                            self.arrDonationList.append(data)
                        }
                    }
                
                    self.getDonorListApi()
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
          //  SVProgressHUD.dismiss()
        }
        
    }
    
    func getDonorListApi()  {
       // SVProgressHUD.show()
        
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_MyDoners! as String + strUserId + "&page=1" as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    if let arrTemp = result.value(forKey: "data") as? NSArray{
                        self.arrDonersList.removeAll()
                        for dict in arrTemp {
                            let data = DonationBaseData.init(data: dict as! [String : Any])
                            self.arrDonersList.append(data)
                        }
                    }
                    self.getVideoListApi()
                
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
         //   SVProgressHUD.dismiss()
        }
        
    }
    
    func getVideoListApi()  {
      //  SVProgressHUD.show()
        
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_UserVideos! as String + strUserId + "&page=1" as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode") as! NSInteger == 200){
                   
                    if let arrTemp = result.value(forKey: "data") as? NSArray{
                        self.arrVideoList.removeAll()
                        for dict in arrTemp {
                            let data = DashboardVideosData.init(data: dict as! [String : Any])
                            self.arrVideoList.append(data)
                        }
                    }
                    self.collecViewList.reloadData()
                
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    

}
