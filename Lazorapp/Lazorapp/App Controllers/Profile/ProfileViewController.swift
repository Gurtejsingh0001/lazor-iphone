//
//  ProfileViewController.swift
//  Lazorapp
//
//  Created by SPDY on 28/05/21.
//

import UIKit

class ProfileViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var tblViewList: UITableView!
    
    
    @IBOutlet weak var imgGreenBG1: UIImageView!
    @IBOutlet weak var imgGreenBG2: UIImageView!
    @IBOutlet weak var imgGreenBG3: UIImageView!
    
    @IBOutlet weak var btnDonationTo: UIButton!
    @IBOutlet weak var btnDonationFrom: UIButton!
    @IBOutlet weak var btnVideos: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    
    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblViewList.dequeueReusableCell(withIdentifier: "MyDonorsTblCell") as! MyDonorsTblCell
        return cell
    }
    

    //MARK:- UIButtons Action
    
    @IBAction func btnEditAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btnDonationToAction(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 0:
            self.imgGreenBG1.isHidden = false
            self.imgGreenBG2.isHidden = true
            self.imgGreenBG3.isHidden = true
            
            btnDonationTo.setTitleColor(UIColor.white, for: .normal)
            btnDonationFrom.setTitleColor(UIColor.black, for: .normal)
            btnVideos.setTitleColor(UIColor.black, for: .normal)
        case 1:
            self.imgGreenBG1.isHidden = true
            self.imgGreenBG2.isHidden = false
            self.imgGreenBG3.isHidden = true
            
            btnDonationTo.setTitleColor(UIColor.black, for: .normal)
            btnDonationFrom.setTitleColor(UIColor.white, for: .normal)
            btnVideos.setTitleColor(UIColor.black, for: .normal)
        case 2:
            self.imgGreenBG1.isHidden = true
            self.imgGreenBG2.isHidden = true
            self.imgGreenBG3.isHidden = false
            
            btnDonationTo.setTitleColor(UIColor.black, for: .normal)
            btnDonationFrom.setTitleColor(UIColor.black, for: .normal)
            btnVideos.setTitleColor(UIColor.white, for: .normal)
        default:
            print("No Tag")
        }
        
//        let vc = storyboard?.instantiateViewController(withIdentifier: "ReelAllDonerViewController") as! ReelAllDonerViewController
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}
