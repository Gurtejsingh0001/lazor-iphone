//
//  EditProfileViewController.swift
//  Lazorapp
//
//  Created by SPDY on 27/05/21.
//

import UIKit
import SVProgressHUD
import Alamofire

class EditProfileViewController: BaseViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    //Personal Details View
    @IBOutlet weak var viewMainPersonalDetails: UIView!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewUserName: UIView!
    @IBOutlet weak var viewUniversity: UIView!
    @IBOutlet weak var viewOccupation: UIView!
    @IBOutlet weak var viewCityState: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewDOB: UIView!
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtUniversity: UITextField!
    @IBOutlet weak var txtOccupation: UITextField!
    @IBOutlet weak var txtCityState: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtDOB: UITextField!
    
    //Banking Inforamation View
    @IBOutlet weak var viewMainBankInformation: UIView!
    @IBOutlet weak var viewBankName: UIView!
    @IBOutlet weak var viewRoutingNumber: UIView!
    @IBOutlet weak var viewAcountNumber: UIView!
    
    @IBOutlet weak var txtBankName: UITextField!
    @IBOutlet weak var txtRoutingNumber: UITextField!
    @IBOutlet weak var txtAcountNumber: UITextField!
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    
    
    
    @IBOutlet weak var btnPersonalDetail: UIButton!
    @IBOutlet weak var btnBankingInfo: UIButton!
    
    @IBOutlet weak var lbl1Line: UILabel!
    @IBOutlet weak var lbl2Line: UILabel!
    
    @IBOutlet weak var viewGradiant: UIView!
    
    @IBOutlet weak var layoutViewMainDetailsHeight: NSLayoutConstraint!
    
    let datePicker = UIDatePicker()

    var imagePicker = UIImagePickerController();
    var imgBase64 = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showDatePicker()
        lbl1Line.layer.backgroundColor = colorGreen.cgColor
        viewName.addGrayViewBorder()
        viewUserName.addGrayViewBorder()
        viewUniversity.addGrayViewBorder()
        viewOccupation.addGrayViewBorder()
        viewCityState.addGrayViewBorder()
        viewEmail.addGrayViewBorder()
        viewDOB.addGrayViewBorder()
        
        viewBankName.addGrayViewBorder()
        viewRoutingNumber.addGrayViewBorder()
        viewAcountNumber.addGrayViewBorder()
        
        let gradient = CAGradientLayer()
        gradient.frame = viewGradiant.bounds
        let color1 = UIColor.clear.withAlphaComponent(0.8).cgColor
        gradient.colors = [(UIColor.clear.cgColor ),color1]
        
        viewGradiant.clipsToBounds = true
        viewGradiant.layer.cornerRadius = 20
        viewGradiant.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
     
        self.viewGradiant.layer.addSublayer(gradient)
        
        getUserPersonalDataApi()
        
        
    }
    
    //MARK:- Text Field Delegtae
    private func switchBasedNextTextField(_ textField: UITextField) {
        switch textField {
        case self.txtName:
            self.txtUserName.becomeFirstResponder()
        case self.txtUserName:
            self.txtCityState.becomeFirstResponder()
//        case self.txtUniversity:
//            self.txtOccupation.becomeFirstResponder()
//        case self.txtOccupation:
//            self.txtCityState.becomeFirstResponder()
        case self.txtCityState:
            self.txtEmail.becomeFirstResponder()
        default:
            self.txtEmail.resignFirstResponder()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.switchBasedNextTextField(textField)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

      
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        return true
    }
    
    //MARK:- DatePicker
    @IBAction func datePickerChanged(_ sender: Any) {
        showDatePicker()
    }
    
    func showDatePicker(){
          //Formate Date
          datePicker.datePickerMode = .date
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }

        datePicker.set18YearValidation()
         //ToolBar
         let toolbar = UIToolbar();
         toolbar.sizeToFit()
         let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
         let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
         let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

       toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

        txtDOB.inputAccessoryView = toolbar
        txtDOB.inputView = datePicker

   }

    @objc func donedatePicker(){
        
         let formatter = DateFormatter()
         formatter.dateFormat = "yyyy/MM/dd"
         txtDOB.text = formatter.string(from: datePicker.date)
         self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
        
    }
    
    
    //MARK:- image Picker functions
    
      func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
              self.present(imagePicker, animated: true, completion: nil)
          }else{
           //   HelperClass.showPopupAlertController(sender: self, message: kCameraIssue, title: kAlertTitle);
                self.showErrorMessage(kCameraIssue)

          }
      }
    
      func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
          self.present(imagePicker, animated: true, completion: nil)
      }
      
    //MARK:- UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        imgUserProfile.contentMode = .scaleToFill
        imgUserProfile.image = tempImage
        
        self.imgBase64 = ConvertImageToBase64String (img: tempImage)
        self.dismiss(animated: true, completion: nil)
    }
     
     func ConvertImageToBase64String (img: UIImage) -> String {
        let imageData:NSData = img.jpegData(compressionQuality: 0.50)! as NSData //UIImagePNGRepresentation(img)
         let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
         return imgString
     }

     func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
         print("picker cancel.")
         dismiss(animated: true, completion: nil)
     }
    
    
    
    //MARK:- UIButtons Action
    
    @IBAction func btnUpdateImageAction(_ sender: Any) {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallary", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func btnPersonalDetailsAction(_ sender: Any) {
        lbl1Line.layer.backgroundColor = colorGreen.cgColor
        btnPersonalDetail.setTitleColor(colorGreen, for: .normal)
        lbl2Line.layer.backgroundColor = UIColor.white.cgColor
        btnBankingInfo.setTitleColor(UIColor.black, for: .normal)
        viewMainPersonalDetails.isHidden = false
        viewMainBankInformation.isHidden = true
        layoutViewMainDetailsHeight.constant = 420
        
    }
    
    @IBAction func btnBankingInfoAction(_ sender: Any) {
        lbl1Line.layer.backgroundColor = UIColor.white.cgColor
        btnPersonalDetail.setTitleColor(UIColor.black, for: .normal)
        lbl2Line.layer.backgroundColor = colorGreen.cgColor
        btnBankingInfo.setTitleColor(colorGreen, for: .normal)
        viewMainPersonalDetails.isHidden = true
        viewMainBankInformation.isHidden = false
        layoutViewMainDetailsHeight.constant = 300
        
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        
        let tag = sender.tag
        if tag == 0{
            if txtName.text?.isEmpty == true{
                self.showErrorMessage(msgName);
            }else if txtUserName.text?.isEmpty == true{
                self.showErrorMessage(msgUserName);
    //        }else if txtUniversity.text?.isEmpty == true{
    //            self.showErrorMessage(msgUniversity);
//            }else if txtOccupation.text?.isEmpty == true{
//                self.showErrorMessage(msgOccupation);
            }else if txtCityState.text?.isEmpty == true{
                self.showErrorMessage(msgAddress);
            }else if txtEmail.text?.isEmpty == true{
                self.showErrorMessage(msgEmail);
            }else if txtDOB.text?.isEmpty == true{
                self.showErrorMessage(msgDOB);
            }else{
                uploadUserDataApi()
            }
        }else{
            
            if txtBankName.text?.isEmpty == true{
                self.showErrorMessage(msgBankName);
            }else if txtRoutingNumber.text?.isEmpty == true{
                self.showErrorMessage(msgRoutingNumber);
            }else if txtAcountNumber.text?.isEmpty == true{
                self.showErrorMessage(msgAccountNumber);
            }else{
                bankDetailsApi()
            }
            
        }

            
    }
    
    
    
    //MARK: - API Methods
    func getUserPersonalDataApi()  {
        SVProgressHUD.show()
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_UserPersonalDetails! as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    let dictData = result.value(forKey: "data") as! NSDictionary
                    
                    self.txtName.text = dictData["name"] as? String
                    self.txtUserName.text = dictData["username"] as? String
                    self.txtUniversity.text = dictData["university"] as? String
                    self.txtOccupation.text = dictData["occupation"] as? String
                    self.txtCityState.text = dictData["address"] as? String
                    self.txtEmail.text = dictData["email"] as? String
                    self.txtDOB.text = dictData["dob"] as? String
                    
                    let imgURL = URL(string: dictData["image"] as? String ?? "")
                    if #available(iOS 13.0, *) {
                        self.imgUserProfile.sd_setImage(with: imgURL, placeholderImage: UIImage(systemName: "person.fill"))
                    } else {
                        self.imgUserProfile.sd_setImage(with: imgURL, placeholderImage:  UIImage(named: "Mask Group 84"))
                    }
                    
                    self.getUserBankDataApi()

                    
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    func getUserBankDataApi()  {
        SVProgressHUD.show()
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_UserBankDetails! as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    let dictData = result.value(forKey: "data") as! NSDictionary
                    
                    self.txtBankName.text = dictData["bank_name"] as? String
                    self.txtRoutingNumber.text = dictData["routing_number"] as? String
                    self.txtAcountNumber.text = dictData["account_number"] as? String

                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    
    func bankDetailsApi()  {
        SVProgressHUD.show()
        
        let dictParam : NSDictionary? =  ["bank_name": txtBankName.text!, "routing_number": txtRoutingNumber.text!, "account_number": txtAcountNumber.text!];
        
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_BankDetails! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    
    func uploadUserDataApi(){

        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["name": txtName.text!, "email": txtEmail.text!, "username": txtUserName.text!, "university": txtUniversity.text ?? "" , "occupation": txtOccupation.text!, "address": txtCityState.text!, "dob": txtDOB.text!];

        print(dictParam as Any)
        let imgDData = imgUserProfile.image!.jpegData(compressionQuality: 0.3)
        
        let accessToken = UserDefaults.standard.string(forKey: kAccessToken)
        let newAcess = accessToken ?? ""
        print(accessToken ?? "")
        
        let newToken = "Bearer " + String(describing: newAcess)
        print(newToken)

        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "cache-control": "no-cache",
            "Authorization": newToken
        ]
        
        let urlRequest = try! URLRequest(url: URL_ProfileEdit! as String, method: .post, headers: headers)
           let multipartFormData = MultipartFormData();
           
           for (key, value) in dictParam! {
            multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!,withName: key as! String)
           }
        let imgData = imgDData
        print(imgData)
        multipartFormData.append(imgData!, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")

//        multipartFormData.append(comment as! Data, withName: "comment", fileName: "\(comment).gif", mimeType: "image/gif")
           AF.upload(multipartFormData: multipartFormData, with: urlRequest).responseJSON
           { (response) in

            print(response.result)

              do{
                  if let jsonData = response.data{
                      let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
                      print(parsedData)
                    
                        let dict =  parsedData["data"] as! NSDictionary
                         UserDefaults.standard.set(dict, forKey: kUserData)
                        UserDefaults.standard.set(dict["id"], forKey: kUserID)
                    self.showErrorMessage(parsedData["message"] as! String)
                      
                  }
              }catch{
                  print("error message")
              }
                
            SVProgressHUD.dismiss()
            

           }

    }
    
    
    
}
