//
//  ReelAllDonerViewController.swift
//  Lazorapp
//
//  Created by SPDY on 28/05/21.
//

import UIKit
import SVProgressHUD

class ReelAllDonerViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var tblViewDoners: UITableView!
    
    @IBOutlet weak var imgVideoDonation: UIImageView!
    @IBOutlet weak var lblTitleVideo: UILabel!
    @IBOutlet weak var lblViewsCount: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblAmountRaised: UILabel!
    @IBOutlet weak var lblAmountOF: UILabel!
    
    
    var arrDonersDetail = [VideoDetailDonerData]()
    var userId = String()
    var strDonationCategory = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getVideoDetailsApi()
    }
    
    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrDonersDetail.count != 0{
            return arrDonersDetail[0].donors.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblViewDoners.dequeueReusableCell(withIdentifier: "MyDonorsTblCell") as! MyDonorsTblCell
        let data = self.arrDonersDetail[0].donors[indexPath.row]
        cell.lblAmount.text = data.amount
        cell.lblCategoryName.text = strDonationCategory
        cell.lblTitle.text = data.donation_by[0].name
        let imgURL = URL(string: data.donation_by[0].image)
        cell.imgView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Mask Group 84"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = self.arrDonersDetail[0].donors[indexPath.row]
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileNewViewController") as! ProfileNewViewController
        vc.strUserId = data.donation_by[0].id
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    //MARK:- UIButtons Action
    @IBAction func btnPlayVideoAction(_ sender: Any) {
        
    }
    
    
    
    //MARK:- Api's Method
    func getVideoDetailsApi()  {
        SVProgressHUD.show()
        print(userId)
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_DonerVideoDetails! as String + userId + "/donors")
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    if let arrTemp = result.value(forKey: "data") as? NSArray{
                        self.arrDonersDetail.removeAll()
                        for dict in arrTemp {
                            let data = VideoDetailDonerData.init(data: dict as! [String : Any])
                            self.arrDonersDetail.append(data)
                        }
                    }
                    
                    if self.arrDonersDetail.count != 0{
                        let data = self.arrDonersDetail[0]
                        self.lblTitleVideo.text = data.caption
                        self.lblCategory.text = data.category[0].name
                        self.lblViewsCount.text = data.views_count
                        self.lblAmountOF.text = "of $" + data.donation_amount
                        self.lblAmountRaised.text = "$" + data.donation_received + " raised"
                        self.strDonationCategory = data.category[0].name
                        
                        let imgURL = URL(string: data.thumbnail)
                        self.imgVideoDonation.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Mask Group 84"))
                    }
                    
                    self.tblViewDoners.reloadData()
                
                   
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }

}
