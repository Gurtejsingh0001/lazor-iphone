//
//  VideoViewController.swift
//  Lazorapp
//
//  Created by Apple on 21/07/21.
//

import UIKit
import SVProgressHUD
import AVKit

class VideoViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {


    @IBOutlet weak var tblVideoList: UITableView!
    
    var arrCommentList = [CommentsBaseData]()
    var viewVideoPlayer: PlayerView!
    var cellHeights: [IndexPath : CGFloat] = [:]
    var videoId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let refreshControl: UIRefreshControl = {
                let refreshControl = UIRefreshControl()
                refreshControl.addTarget(self, action:
                             #selector(self.handleRefresh(_:)),
                                         for: UIControl.Event.valueChanged)
                refreshControl.tintColor = UIColor.black
                
                return refreshControl
            }()
        
        self.tblVideoList.addSubview(refreshControl)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCommentListApi()
    }
  
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
           
        getCommentListApi()
        refreshControl.endRefreshing()
    }
    
    
    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCommentList.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblVideoList.dequeueReusableCell(withIdentifier: "DashBoardTblCell") as! DashBoardTblCell
        let data = arrCommentList[indexPath.row]
        cell.lblShare.text = data.shares_count
        cell.lblViews.text = data.views_count
        cell.lblComments.text = data.comments_count
        cell.lblUserName.text = data.user[0].name
        cell.lblUserLocation.text = data.user[0].address
        if data.is_prime == "No"{
            cell.btnIsPrimeum.isHidden = true
        }else{
            cell.btnIsPrimeum.isHidden = false
        }
        
        let imgURL = URL(string: data.thumbnail)!
        cell.imgBGThumbnail.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Group 3645"))
        

        let playerItem = AVPlayerItem(url: imgURL)
        let videoURL = URL(string: data.video)

        cell.viewVideoPlayer.player = AVPlayer(playerItem: playerItem)
        cell.viewVideoPlayer.player = AVPlayer(url: videoURL! as URL)
        self.viewVideoPlayer = cell.viewVideoPlayer
        self.viewVideoPlayer.player?.play()
        
        
      
        
        let imgUserURL = URL(string: data.user[0].image) ?? URL(string:"")
        cell.btnUserImage.sd_setBackgroundImage(with: imgUserURL, for: .normal, placeholderImage: UIImage(named: "Mask Group 84"))
        cell.btnShare.tag = indexPath.row
        cell.btnShare.addTarget(self, action: #selector(btnShareAction(_:)), for: .touchUpInside)
        cell.btnComments.tag = indexPath.row
        cell.btnComments.addTarget(self, action: #selector(btnCommentsAction(_:)), for: .touchUpInside)
        cell.btnLike.tag = indexPath.row
        cell.btnLike.addTarget(self, action: #selector(btnLikePostAction(_:)), for: .touchUpInside)
        cell.btnUserImage.tag = indexPath.row
        cell.btnUserImage.addTarget(self, action: #selector(btnUserProfileAction (_:)), for: .touchUpInside)
        
        if data.wishlist_count == "1"{
            cell.btnLike.setImage(UIImage.init(named: "Group 3644"), for: .normal)
        }else{
            cell.btnLike.setImage(UIImage.init(named: "Group 3724"), for: .normal)
        }
        
        cell.imgBGThumbnail.isHidden = true
        
        return cell
    }
    

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//           guard let videoCell = cell as? VideoTableViewCell else { return };
           
        let cell = self.tblVideoList.dequeueReusableCell(withIdentifier: "DashBoardTblCell") as! DashBoardTblCell
        
            self.viewVideoPlayer = cell.viewVideoPlayer
            self.viewVideoPlayer.player?.pause()
            self.viewVideoPlayer.player = nil
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.bounds.size.height;
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       
        let cell = self.tblVideoList.dequeueReusableCell(withIdentifier: "DashBoardTblCell") as! DashBoardTblCell
         self.viewVideoPlayer = cell.viewVideoPlayer
        cellHeights[indexPath] = self.view.frame.size.height
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? self.view.frame.size.height
    }
    
    
    //MARK:- UIButtons Action
    @IBAction func btnSideMenuAction(_ sender: UIButton) {

        self.viewVideoPlayer.player?.pause()
        self.viewVideoPlayer.player = nil
        self.revealViewController().revealToggle(sender)
        
    }
    
    @IBAction func btnPlayAction(_ sender: Any) {
        
        self.viewVideoPlayer.player?.play()
    }
    
    
    @IBAction func btnLikePostAction(_ sender: UIButton) {

        let tag = sender.tag
        let data = arrCommentList[tag]
        
        if data.wishlist_count == "0"{
            data.wishlist_count = "1"
            self.addWishlistApi(requestID: data.id)
            let messageVC = UIAlertController(title: "", message: "Moved to holding area" , preferredStyle: .actionSheet)
            present(messageVC, animated: true) {
                            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (_) in
                                messageVC.dismiss(animated: true, completion: nil)})}
        }else{
            data.wishlist_count = "0"
            self.removeWishlistApi(requestID: data.id)
            let messageVC = UIAlertController(title: "", message: "Remove from holding area" , preferredStyle: .actionSheet)
            present(messageVC, animated: true) {
                            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (_) in
                                messageVC.dismiss(animated: true, completion: nil)})}
        }
        
        self.tblVideoList.reloadData()
    }
    
    @IBAction func btnShareAction(_ sender: UIButton) {

        // Setting description
//         let firstActivityItem = "Lazor App.."
//
//         // Setting url
//         let secondActivityItem : NSURL = NSURL(string: "LazorApp.com/")!
//         
//         // If you want to use an image
//         let image : UIImage = UIImage(named: "Group 3645")!
//         let activityViewController : UIActivityViewController = UIActivityViewController(
//             activityItems: [firstActivityItem, secondActivityItem, image], applicationActivities: nil)
        
        let someText:String = "com.Lazor://main"
        let objectsToShare:URL = URL(string: "com.Lazor://main")!
        let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
        let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
         
         // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = (sender )
         
         // This line remove the arrow of the popover to show in iPad
         activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
         activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
         
         // Pre-configuring activity items
        if #available(iOS 13.0, *) {
            activityViewController.activityItemsConfiguration = [
                UIActivity.ActivityType.message
            ] as? UIActivityItemsConfigurationReading
        } else {
            // Fallback on earlier versions
        }
         
         // Anything you want to exclude
         activityViewController.excludedActivityTypes = [
             UIActivity.ActivityType.postToWeibo,
             UIActivity.ActivityType.print,
             UIActivity.ActivityType.assignToContact,
             UIActivity.ActivityType.saveToCameraRoll,
             UIActivity.ActivityType.addToReadingList,
             UIActivity.ActivityType.postToFlickr,
             UIActivity.ActivityType.postToVimeo,
             UIActivity.ActivityType.postToTencentWeibo,
             UIActivity.ActivityType.postToFacebook
         ]
         
        if #available(iOS 13.0, *) {
            activityViewController.isModalInPresentation = true
        } else {
            // Fallback on earlier versions
        }
         self.present(activityViewController, animated: true, completion: nil)
        
        
    }
    
    @IBAction func btnUserProfileAction(_ sender: UIButton) {

        self.viewVideoPlayer.player?.pause()
        self.viewVideoPlayer.player = nil
        let tag = sender.tag
        let storyboard = staticClass.getStoryboard_Profile()
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileNewViewController") as! ProfileNewViewController
        vc.strUserId = arrCommentList[tag].user[0].id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnCommentsAction(_ sender: UIButton) {
        let tag = sender.tag
        let storyboard = staticClass.getStoryboard_Main()
        let vc = storyboard?.instantiateViewController(withIdentifier: "CommentsViewController") as! CommentsViewController
        vc.arrCommentList = self.arrCommentList
        vc.postId = self.arrCommentList[tag].id
        self.present(vc, animated: true, completion: nil)
    }
    
    
    //MARK: - API Methods
    func getCommentListApi()  {
        SVProgressHUD.show()
        
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_CommentsList! as String + "/" + videoId as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    if let arrTemp = result.value(forKey: "data") as? NSArray{
                        self.arrCommentList.removeAll()
                        for dict in arrTemp {
                            let data = CommentsBaseData.init(data: dict as! [String : Any])
                            self.arrCommentList.append(data)
                        }
                    }
                
                    self.tblVideoList.reloadData()
                  
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    func addWishlistApi(requestID: String)  {
        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["donation_request_id": requestID];
        
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_AddWishlist! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                    
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    func removeWishlistApi(requestID: String)  {
        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["donation_request_id": requestID];
        
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_RemoveWishlist! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                    
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
            }
            SVProgressHUD.dismiss()
        }
        
    }

    
    
}
