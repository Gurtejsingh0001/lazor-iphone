//
//  ForgotPasswordVC1.swift
//  Lazorapp
//
//  Created by Apple on 12/07/21.
//

import UIKit
import SVProgressHUD
import CountryPickerView

class ForgotPasswordVC1: BaseViewController, CountryPickerViewDelegate, CountryPickerViewDataSource,UITextFieldDelegate {

    @IBOutlet weak var txtMobileNum: UITextField!
    @IBOutlet weak var viewMobileNum: UIView!
    
    @IBOutlet weak var countryPickerView: CountryPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewMobileNum.addGrayViewBorder()
        
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        
        countryPickerView.setCountryByPhoneCode("+91")
    }
    
    //MARK:- UITextFields Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
      
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == txtMobileNum {
            let newLength: Int = (textField.text?.count)! + (string.count ) - range.length
            let csTxt = CharacterSet(charactersIn: ACCEPTABLE_MOBILE_NUMBER).inverted
            let filteredUsrTxt: String = string.components(separatedBy: csTxt).joined(separator: "")
            if (string == filteredUsrTxt) {
                if newLength > 10 {
                    return false
                }
            }
            else {
                return false
            }
            let length: Int = HelperClass.getLength(mobileNumber: textField.text!)
            //NSLog(@"Length  =  %d ",length);
            if length >= 10 {
                if range.length == 0 {
                    return false
                }
            }
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        return true
    }
    
    //MARK:- UICountryPicker
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        
        countryPickerView.getCountryByName(country.name)
//        countryPickerView.getCountryByCode(country.code)
//        countryPickerView.getCountryByPhoneCode(country.phoneCode)
    }
    
    //MARK:- UIButtons Action
    @IBAction func btnContinueAction(_ sender: Any) {
        if txtMobileNum.text?.isEmpty == true{
            self.showErrorMessage(msgMobile);
        }else{
            forgotPasswordApi()
        }
        
    }
    
    
    //MARK: - API Methods
    func forgotPasswordApi()  {
        print(txtMobileNum.text as Any)
        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["country_code": "91", "phone": txtMobileNum.text!];
    
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_PasswordReset! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   let data = result.value(forKey: "data")as! NSDictionary
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyCodeViewController") as! VerifyCodeViewController
                    vc.strController = "Forgot"
                    vc.phoneNumber = data["phone"] as! String;
                    vc.token = data["token"] as! String;
                    self.navigationController?.pushViewController(vc, animated: true)
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                  
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
               
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }

}
