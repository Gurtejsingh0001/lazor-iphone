//
//  ForgotPasswordViewController.swift
//  Lazorapp
//
//  Created by SPDY on 28/05/21.
//

import UIKit
import SVProgressHUD

class ForgotPasswordViewController: BaseViewController {

    
    @IBOutlet weak var viewNewPassword: UIView!
    @IBOutlet weak var viewConfirmPassword: UIView!
    
    
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    var token = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewNewPassword.addGrayViewBorder()
        viewConfirmPassword.addGrayViewBorder()
       
    }
    
    //MARK:- UIButtons Action
    @IBAction func btnSubmitAction(_ sender: Any) {
    
        if txtNewPassword.text?.isEmpty == true{
            self.showErrorMessage(msgPassword);
        }else if txtConfirmPassword.text?.isEmpty == true{
            self.showErrorMessage(msgConfirmPassword);
        }else if txtNewPassword.text != txtConfirmPassword.text{
            self.showErrorMessage(msgPasswordMismatch);
        }else{
            ForgotPasswordApi()
        }
        
        
    }
    

    //MARK: - API Methods
    func ForgotPasswordApi()  {
        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["token": token, "password": txtNewPassword.text!, "password_confirmation": txtConfirmPassword.text!];
    
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_UpdatePassword! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
        
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                    vc.strControllerType = "Forgot"
                    self.navigationController?.pushViewController(vc, animated: true)
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                  
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
               
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }

}
