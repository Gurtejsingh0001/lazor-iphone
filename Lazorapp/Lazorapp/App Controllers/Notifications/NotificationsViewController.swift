//
//  NotificationsViewController.swift
//  Lazorapp
//
//  Created by SPDY on 28/05/21.
//

import UIKit
import SVProgressHUD

class NotificationsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblNotificationList: UITableView!
    
    var arrNotificationList = [NotificationsMainData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNotificationListApi()
        
    }
    
    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotificationList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblNotificationList.dequeueReusableCell(withIdentifier: "NotificationsTblCell") as! NotificationsTblCell
        let data = self.arrNotificationList[indexPath.row]
        cell.lblNotificationTitle.text = data.notification[0].title
        cell.lblNotificationTime.text = data.created_at
        return cell
    }
    

    //MARK: - API Methods
    func getNotificationListApi()  {
        SVProgressHUD.show()
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_Notifications! as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    if let arrTemp = result.value(forKey: "data") as? NSArray{
                        self.arrNotificationList.removeAll()
                        for dict in arrTemp {
                            let data = NotificationsMainData.init(data: dict as! [String : Any])
                            self.arrNotificationList.append(data)
                        }
                        self.tblNotificationList.reloadData()
                    }
                   
                    if self.arrNotificationList.count == 0{
                        self.tblNotificationList.isHidden = true
                    }
                    
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    
}
