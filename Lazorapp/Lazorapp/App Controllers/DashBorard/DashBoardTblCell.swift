//
//  DashBoardTblCell.swift
//  Lazorapp
//
//  Created by SPDY on 29/05/21.
//

import UIKit

class DashBoardTblCell: UITableViewCell {

    @IBOutlet weak var viewVideoPlayer: PlayerView!
    @IBOutlet weak var imgBGThumbnail: UIImageView!
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewGradiant: UIView!
    @IBOutlet weak var viewGradiantTop: UIView!
    
    @IBOutlet weak var lblHoldingArea: UILabel!
    @IBOutlet weak var lblShare: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var lblViews: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserLocation: UILabel!
    @IBOutlet weak var btnUserImage: UIButton!
    
    @IBOutlet weak var btnIsPrimeum: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnComments: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let gradient = CAGradientLayer()
        gradient.frame = viewGradiant.bounds
        let colorBlack = UIColor.black.withAlphaComponent(1).cgColor
        let color1 = UIColor.clear.withAlphaComponent(0.5).cgColor
        gradient.colors = [UIColor.clear.cgColor,color1,colorBlack]
     
        self.viewGradiant.layer.addSublayer(gradient)
        lblHoldingArea.text = "  Add,Holding Area."
//        UIView.animate(withDuration: 3.0, delay: 0.0, options: ([.repeat, .repeat]), animations: {() -> Void in
//            self.lblHoldingArea.center = CGPoint(x: 0 - self.lblHoldingArea.bounds.size.width, y: self.lblHoldingArea.center.y)
//                }, completion:  { _ in })
        
        _ = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(marquee), userInfo: nil, repeats: true)
        
        let gradientTop = CAGradientLayer()
        gradientTop.frame = viewGradiantTop.bounds
      //  let colorBlack = UIColor.black.withAlphaComponent(1).cgColor
     //   let color1 = UIColor.clear.withAlphaComponent(0.5).cgColor
        gradientTop.colors = [colorBlack,color1,(UIColor.clear.cgColor )]
     
        self.viewGradiantTop.layer.addSublayer(gradientTop)
        
    }
    
    @objc func marquee(){

      let str = lblHoldingArea.text!
      let indexFirst = str.index(str.startIndex, offsetBy: 0)
      let indexSecond = str.index(str.startIndex, offsetBy: 1)
        lblHoldingArea.text = String(str.suffix(from: indexSecond)) + String(str[indexFirst])

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
