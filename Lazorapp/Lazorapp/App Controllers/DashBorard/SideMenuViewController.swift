//
//  SideMenuViewController.swift
//  Lazorapp
//
//  Created by SPDY on 30/05/21.
//

import UIKit

class SideMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var tblViewSideMenu: UITableView!
    @IBOutlet weak var viewMainTop: UIView!
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!
    
    
    var userId = Int()
    
    
    let arrSideMenuTItle = ["Donation Request", "My Subscription", "My Holding Area", "My Donation", "My Donors", "Cash Out", "Lazor News", "Notifications", "Rate the App", "Invite", "Settings"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        viewMainTop.clipsToBounds = true
        viewMainTop.layer.cornerRadius = 20
        viewMainTop.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.revealViewController().revealToggle(animated)
        
        let data = UserDefaults.standard.dictionary(forKey: kUserData)
        print(data as Any)
        lblUserName.text = data?["name"] as? String
        lblUserEmail.text = data?["email"] as? String
     //   userId = Int((data?["id"] as? Any)! as! String)!
        
        userId = UserDefaults.standard.integer(forKey: kUserID)
        
        let imgURL = URL(string: data?["image"] as? String ?? "")
        if #available(iOS 13.0, *) {
            imgUserProfile.sd_setImage(with: imgURL, placeholderImage: UIImage(systemName: "person.fill"))
        } else {
            imgUserProfile.sd_setImage(with: imgURL, placeholderImage:  UIImage(named: "Mask Group 84"))
        }
        
    }
    
    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSideMenuTItle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblViewSideMenu.dequeueReusableCell(withIdentifier: "SideMenuTblCell") as! SideMenuTblCell
        cell.lblTitle.text = arrSideMenuTItle[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
    
        case 0:
            let storyboard = staticClass.getStoryboard_DonetionRequest()
            let vc = storyboard?.instantiateViewController(withIdentifier: "DonetionRequestViewController") as! DonetionRequestViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 1:
            let storyboard = staticClass.getStoryboard_MySubscription()
            let vc = storyboard?.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 2:
            let storyboard = staticClass.getStoryboard_MyHoldingArea()
            let vc = storyboard?.instantiateViewController(withIdentifier: "MyHoldingAreaViewController") as! MyHoldingAreaViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 3:
            let storyboard = staticClass.getStoryboard_MyDonetions()
            let vc = storyboard?.instantiateViewController(withIdentifier: "MyDonationsViewController") as! MyDonationsViewController
            vc.strUserId = "\(userId)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 4:
            let storyboard = staticClass.getStoryboard_MyDonors()
            let vc = storyboard?.instantiateViewController(withIdentifier: "MyDonorsViewController") as! MyDonorsViewController
            vc.strUserId = "\(userId)"
            self.navigationController?.pushViewController(vc, animated: true)
        case 5:
            let storyboard = staticClass.getStoryboard_CashOut()
            let vc = storyboard?.instantiateViewController(withIdentifier: "CashOutViewController") as! CashOutViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 6:
            let storyboard = staticClass.getStoryboard_LazorNews()
            let vc = storyboard?.instantiateViewController(withIdentifier: "LazorNewsViewController") as! LazorNewsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 7:
            let storyboard = staticClass.getStoryboard_Notifications()
            let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
            self.navigationController?.pushViewController(vc, animated: true)
  //      case 8:
//            let storyboard = staticClass.getStoryboard_Profile()
//            let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileNewViewController") as! ProfileNewViewController
//            vc.strUserId = "\(userId)"
//            self.navigationController?.pushViewController(vc, animated: true)
        case 9:
            let storyboard = staticClass.getStoryboard_Invite()
            let vc = storyboard?.instantiateViewController(withIdentifier: "InviteViewController") as! InviteViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 10:
            let storyboard = staticClass.getStoryboard_Settings()
            let vc = storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            print("...")
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    
    //MARK:- UIButtons Action
    
    @IBAction func btnCloseSideMenuAction(_ sender: Any) {
        self.revealViewController().revealToggle(sender)
    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        let storyboard = staticClass.getStoryboard_Profile()
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileNewViewController") as! ProfileNewViewController
        let data = UserDefaults.standard.dictionary(forKey: kUserData)
        print(data as Any)
       // userId = Int((data?["id"] as? String)!)!
       // userId = Int((data?["id"] as? Any)! as! String)!
        userId = UserDefaults.standard.integer(forKey: kUserID)
        vc.strUserId = "\(userId)"
        vc.strController = "userProfile"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        
        self.revealViewController().revealToggle(sender)
        self.showLogoutAlertPopup(sender: self, message: msgLogout, title: kAlertTitle)
        
        
//        UIControl().sendAction(#selector(NSXPCConnection.suspend),
//                               to: UIApplication.shared, for: nil)
    }
    
    
    func showLogoutAlertPopup(sender : Any, message : String, title : String) -> Void{
        
        let alert = UIAlertController(title: title as String,
                                      message: message as String,
                                      preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style:.default) {
              UIAlertAction in
              NSLog("OK Pressed")
            UserDefaults.standard.removeObject(forKey: kUserData)
            let storyboard = staticClass.getStoryboard_PreLogin()
            let vc = storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
            vc.strControllerType = "Logout"
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
       
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel, handler: nil)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        UIApplication.shared.keyWindow?.rootViewController!.present(alert, animated: true,completion: nil)
    
    }

    
}
