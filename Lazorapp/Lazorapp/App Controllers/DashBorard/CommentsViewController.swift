//
//  CommentsViewController.swift
//  Lazorapp
//
//  Created by SPDY on 31/05/21.
//

import UIKit
import SVProgressHUD
import GiphyUISDK
import Alamofire
import SwiftyGif
import SwiftGifOrigin

class CommentsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,GiphyDelegate {

    @IBOutlet weak var tblViewComments: UITableView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewBGMain: UIView!
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewAddComment: UIView!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var txtAddComment: UITextField!
    
    var arrCommentList = [CommentsBaseData]()
    var arrComments = [CommentData]()
    var postId = String()
    var tagId = String()
    var parentId = String()
    var gifData = Data()
    
    
    let giphy = GiphyViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialize Tap Gesture Recognizer
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapView(_:)))
        viewBGMain.addGestureRecognizer(tapGestureRecognizer)
        
        viewMain.clipsToBounds = true
        viewMain.layer.cornerRadius = 30
        viewMain.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]

        viewAddComment.layer.borderWidth = 1
        viewAddComment.layer.borderColor = colorLightGray.cgColor
        
            // getCommentListApi()
        if self.arrCommentList.count != 0{
            self.tblViewComments.reloadData()
            let imgURL = URL(string: self.arrCommentList[0].user[0].image)
           // self.imgUser.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Group 3645"))
            if #available(iOS 13.0, *) {
                self.imgUser.sd_setImage(with: imgURL, placeholderImage: UIImage(systemName: "person.fill"))
            } else {
                self.imgUser.sd_setImage(with: imgURL, placeholderImage:  UIImage(named: "Mask Group 84"))
            }
        }
        
        Giphy.configure(apiKey: "dc6zaTOxFJmzC")
        
        giphy.mediaTypeConfig = [.gifs]
        GPHCache.shared.cache.diskCapacity = 1 * 1000 * 1000
        GPHCache.shared.cache.memoryCapacity = 1 * 1000 * 1000
        giphy.delegate = self
    
        
    }
    
    //MARK:- UITextView Delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       
        return true
    }
    
    //MARK:- Giphy delegates
    func didSelectMedia(giphyViewController: GiphyViewController, media: GPHMedia)   {
        
        print(media.url)
        let gifURL = media.url(rendition: .fixedWidth, fileType: .gif)!
        print(gifURL)
     //   let url1 = URL(string: gifURL)
        
        
      //  guard let url = media.url(rendition: .fixedWidth, fileType: .webp) else { return }
        GPHCache.shared.cache.diskCapacity = 1 * 1000 * 1000
        GPHCache.shared.cache.memoryCapacity = 1 * 1000 * 1000
        GPHCache.shared.downloadAssetData(gifURL) { (data, error) in
            print(data?.base64EncodedData() as Any)
            self.gifData = data!
            self.uploadGifData(commentsType: "image", comment: self.gifData)
        }
        
        
        giphyViewController.dismiss(animated: true, completion: nil)
    }
    
    func didDismiss(controller: GiphyViewController?) {
         // your user dismissed the controller without selecting a GIF.
    }
    
    // MARK: - Actions

    @IBAction func didTapView(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrCommentList.count != 0{
            return arrCommentList[0].comment.count
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblViewComments.dequeueReusableCell(withIdentifier: "CommentsTblCell") as! CommentsTblCell
        
        let data = arrCommentList[0].comment[indexPath.row]
        cell.lblName.text = data.comment_user[0].name
        let imgURL = URL(string: data.comment_user[0].image)
       // cell.imgUser.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Group 3645"))
        if #available(iOS 13.0, *) {
            cell.imgUser.sd_setImage(with: imgURL, placeholderImage: UIImage(systemName: "person.fill"))
        } else {
            cell.imgUser.sd_setImage(with: imgURL, placeholderImage:  UIImage(named: "Mask Group 84"))
        }
        
        cell.btnRiply.tag = indexPath.row
        cell.btnRiply.addTarget(self, action: #selector(btnRiplyAction(_:)), for: .touchUpInside)
        
        if data.comment_type == "text"{
            cell.imgGifRiply.isHidden = true
            cell.lblComments.isHidden = false
            cell.lblComments.text = data.comment
            cell.layoutGifRiplyHeight.constant = 20
        }else{
             if data.comment != ""{
                print(data.comment)
               if data.comment_type == "image"{
                cell.imgGifRiply.isHidden = false
                cell.lblComments.isHidden = true
                    cell.layoutGifRiplyHeight.constant = 60
                    let gifURL : String = data.comment
                    print(gifURL)
                    cell.imgGifRiply.image = UIImage.gifImageWithURL(gifURL)
                   
                }
             }
        }
        
        cell.arrCommentsRiply = data.replies
        cell.tblViewCommentsRiply.reloadData()

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    //MARK:- UIButtons Action
    @IBAction func btnRemoveAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnRiplyAction(_ sender: UIButton) {
       
        let tag = sender.tag
        print(tag)
        //let userName = arrCommentList[0].comment[tag].comment_user[0].name
        self.parentId = arrCommentList[0].comment[tag].id
        
        
        self.txtAddComment.becomeFirstResponder()
     //   self.txtAddComment.text = "@" + "\(userName)"
        
        
       
    }
    
    
    @IBAction func btnGifAction(_ sender: UIButton) {
        present(giphy, animated: true, completion: nil)
    }
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        sendCommentApi(commentsType: "text", comment: txtAddComment.text ?? "")
    }
    
    
    //MARK: - API Methods
    func getCommentListApi()  {
        SVProgressHUD.show()
        
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_CommentsList! as String + "/" + postId as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    if let arrTemp = result.value(forKey: "data") as? NSArray{
                        self.arrCommentList.removeAll()
                        for dict in arrTemp {
                            let data = CommentsBaseData.init(data: dict as! [String : Any])
                            self.arrCommentList.append(data)
                        }
                        self.tblViewComments.reloadData()
                    }
                
                   
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    func sendCommentApi(commentsType: String, comment: String)  {
        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["comment": comment, "parent_id": parentId, "tag_id": tagId, "comment_type": commentsType, "donation_request_id": postId];
        
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_Comments! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                    self.txtAddComment.text = ""
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                    self.getCommentListApi()
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    

    func uploadGifData(commentsType: String, comment: Any){

        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["parent_id": parentId, "tag_id": tagId, "comment_type": commentsType, "donation_request_id": postId];

        print(dictParam as Any)
        
        let accessToken = UserDefaults.standard.string(forKey: kAccessToken)
        let newAcess = accessToken ?? ""
        print(accessToken ?? "")
        
        let newToken = "Bearer " + String(describing: newAcess)
        print(newToken)

        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "cache-control": "no-cache",
            "Authorization": newToken
                
//                "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOTE4YWZlOTM0ZTJjMDIxOTEyZmU3ZWJhMGE0MWQ0YjkxZmI3ZDU2MjNiZTVkOWI5YWI3OTc2NjY4ODRmMTdkODc5NDJmMGQ1YTc1NDM4MGQiLCJpYXQiOjE2MjM4MzA1NjAuMzgxMjgsIm5iZiI6MTYyMzgzMDU2MC4zODEzMSwiZXhwIjoxNjU1MzY2NTYwLjMwNDIxNywic3ViIjoiMSIsInNjb3BlcyI6W119.Sc0n9xy3jzNztX4Uqle5KR936WawAz2A9H4F8r3CuEXDXgRG8Yg7uh6xlig-n1cxKkoIcm5fqCQ4Dz-V2qvDl9uMPQYS_kV14kVAg8vnkZ_JkZO0zsLG6wNMetuGltFrSsizKcAGOeOFU4BoIxV3RudhQhrh1Yl0YU_zMShAHr2B-R7NElozKBzJhy9PEtWV7dU8LpUy4lFuV6CK3OTJ8jjseUxf7osQgWUf8VlmYG3uGm3oghM2j1355SkEHKaybM_EIiiVYR1uFX2SNWt6D-yhCpIWFKN2zi57e4eeFDLYIM64PEGtZkv_osQaFI6alTdEFqv1j7jk4DFOuMBSV9CjAtjGOqwLJXB04bcdoJIZwOfrz-aMDvtIeyCRMbQtfSf6-VF7lJ6R2-SqXJVjAu_Tybph2A_QFO57N9jEmrW980Iw4noW5_TdVPx0vwZcvbXmDZQHHWY0uh2sCzOqUxHUDEA-OYT2B6l3ZfgJQnkLcqLHT7uOAHWjUu2x3-W-H0Vor34q-sftii_upAmBb_YWGWtemvLGpbh50D0DZnFp1IF7haOefRZVt705uEvtcdAOD3Nk68kk3I16mG_0Gv2COqU8JaOiPnvjMdprsN7JsdDmZrZHnTA40B6urwGBdDbzyUrIJ9FiUxqsLCzCUP6FmneIOTBUxWK7NG3bfl8",
        ]
        
        let urlRequest = try! URLRequest(url: URL_Comments! as String, method: .post, headers: headers)
           let multipartFormData = MultipartFormData();
           
           for (key, value) in dictParam! {
            multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!,withName: key as! String)
           }
        
        multipartFormData.append(comment as! Data, withName: "comment", fileName: "file.zip", mimeType: "application/zip")
        
//        multipartFormData.append(comment as! Data, withName: "comment", fileName: "\(comment).gif", mimeType: "image/gif")
           AF.upload(multipartFormData: multipartFormData, with: urlRequest).responseJSON
           { (response) in

            print(response)
           // self.showErrorMessage(response.value(forKey: "message") as! String)
            self.getCommentListApi()
            SVProgressHUD.dismiss()
            

           }

    }
    
 
}
