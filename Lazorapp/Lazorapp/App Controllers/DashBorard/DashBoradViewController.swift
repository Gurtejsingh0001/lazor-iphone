//
//  DashBoradViewController.swift
//  Lazorapp
//
//  Created by SPDY on 29/05/21.
//

import UIKit
import SVProgressHUD
import AVKit

class DashBoradViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,AVPlayerViewControllerDelegate{

    @IBOutlet weak var tblViewDashBoard: UITableView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var viewMainTop: UIView!
    
    //   var privateList = [DashboardVideosData]()
    //let totalItems = 100 // server does not provide totalItems
    
    var cellHeights: [IndexPath : CGFloat] = [:]
    var arrDashboardVideosList = [DashboardVideosData]()
    var arrDashboardVideosList2 = [DashboardVideosData]()
    var arrCommentList = [CommentsBaseData]()
    var viewVideoPlayer: PlayerView!
    var pageID = 1
    var fromIndex = 0
    var isLike = Bool()
    
    var currentPage : Int = 1
    var isLoadingList : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

      //  getDashboardVideosApi()
        
        if self.revealViewController() != nil {
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController()?.rearViewRevealWidth = 270
        }

        let refreshControl: UIRefreshControl = {
                let refreshControl = UIRefreshControl()
                refreshControl.addTarget(self, action:
                             #selector(self.handleRefresh(_:)),
                                         for: UIControl.Event.valueChanged)
                refreshControl.tintColor = UIColor.black
                
                return refreshControl
            }()
        
        self.tblViewDashBoard.addSubview(refreshControl)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let data = UserDefaults.standard.dictionary(forKey: kUserData)
        print(data as Any)
        lblUserName.text = data?["name"] as? String
     //   pageID  = 1
     //   privateList.removeAll()
        self.arrDashboardVideosList.removeAll()
        getDashboardVideosApi()
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
           
        currentPage = 1
        self.arrDashboardVideosList.removeAll()
        getDashboardVideosApi()
   //     loadPrevItemsForList()
        refreshControl.endRefreshing()
    }
    
//    func loadPrevItemsForList(){
//        if currentPage > 1{
//            currentPage = currentPage - 1
//        }
//        getDashboardVideosApi()
//       // self.tblViewDashBoard.layoutIfNeeded()
//     //   self.tblViewDashBoard.contentOffset = CGPoint(x: 0, y: -self.tblViewDashBoard.contentInset.bottom)
//    }
   
    func loadMoreItemsForList(){
        currentPage += 1
        getDashboardVideosApi()
    //    self.tblViewDashBoard.layoutIfNeeded()
       // self.tblViewDashBoard.contentOffset = CGPoint(x: 0, y: -self.tblViewDashBoard.contentInset.top)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
         if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){
             self.isLoadingList = true
             self.loadMoreItemsForList()
         }
     }

    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDashboardVideosList.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblViewDashBoard.dequeueReusableCell(withIdentifier: "DashBoardTblCell") as! DashBoardTblCell
        let data = arrDashboardVideosList[indexPath.row]
        cell.lblShare.text = data.shares_count
        cell.lblViews.text = data.views_count
        cell.lblComments.text = data.comments_count
        cell.lblUserName.text = data.user[0].name
        cell.lblUserLocation.text = data.user[0].address
//        if data.is_prime == "No"{
//            cell.btnIsPrimeum.isHidden = true
//        }else{
//            cell.btnIsPrimeum.isHidden = false
//        }
        
        let imgURL = URL(string: data.thumbnail)!
        cell.imgBGThumbnail.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Group 3645"))
        

        let playerItem = AVPlayerItem(url: imgURL)
        let videoURL = URL(string: data.video)

        cell.viewVideoPlayer.player = AVPlayer(playerItem: playerItem)
        cell.viewVideoPlayer.player = AVPlayer(url: videoURL! as URL)
        self.viewVideoPlayer = cell.viewVideoPlayer
        self.viewVideoPlayer.player?.play()
        
       // let player = AVPlayer(url: videoURL! as URL)
   //     let playerLayer = AVPlayerLayer(player: player)
  //      playerLayer.frame = cell.viewVideoPlayer.bounds
  //      cell.viewVideoPlayer.layer.addSublayer(playerLayer)
        //self.btnPlay.isHidden = true
      //  cell.viewVideoPlayer.player?.play()
      //  player.play()
        
        
        let imgUserURL = URL(string: data.user[0].image) ?? URL(string:"")
        cell.btnUserImage.sd_setBackgroundImage(with: imgUserURL, for: .normal, placeholderImage: UIImage(named: "Mask Group 84"))
        cell.btnShare.tag = indexPath.row
        cell.btnShare.addTarget(self, action: #selector(btnShareAction(_:)), for: .touchUpInside)
        cell.btnComments.tag = indexPath.row
        cell.btnComments.addTarget(self, action: #selector(btnCommentsAction(_:)), for: .touchUpInside)
        cell.btnLike.tag = indexPath.row
        cell.btnLike.addTarget(self, action: #selector(btnLikePostAction(_:)), for: .touchUpInside)
        cell.btnUserImage.tag = indexPath.row
        cell.btnUserImage.addTarget(self, action: #selector(btnUserProfileAction (_:)), for: .touchUpInside)
        
        if data.wishlist_count == "1"{
            cell.btnLike.setImage(UIImage.init(named: "Group 3644"), for: .normal)
        }else{
            cell.btnLike.setImage(UIImage.init(named: "Group 3724"), for: .normal)
        }
        
        cell.imgBGThumbnail.isHidden = true
        
//        if indexPath.row == privateList.count - 1 {
//
//            pageID += 1
//            getDashboardVideosApi()
//
//        }
        return cell
    }
    

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//           guard let videoCell = cell as? VideoTableViewCell else { return };
           
        let cell = self.tblViewDashBoard.dequeueReusableCell(withIdentifier: "DashBoardTblCell") as! DashBoardTblCell
        
            self.viewVideoPlayer = cell.viewVideoPlayer
            self.viewVideoPlayer.player?.pause()
            self.viewVideoPlayer.player = nil
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.bounds.size.height;
    }
    
    


    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       
        let cell = self.tblViewDashBoard.dequeueReusableCell(withIdentifier: "DashBoardTblCell") as! DashBoardTblCell
         self.viewVideoPlayer = cell.viewVideoPlayer
        cellHeights[indexPath] = self.view.frame.size.height
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? self.view.frame.size.height
    }
    
    
    //MARK:- UIButtons Action
    @IBAction func btnSideMenuAction(_ sender: UIButton) {

        if self.viewVideoPlayer.player != nil{
            self.viewVideoPlayer.player?.pause()
            self.viewVideoPlayer.player = nil
        }
        self.revealViewController().revealToggle(sender)
        
    }
    
    @IBAction func btnLikePostAction(_ sender: UIButton) {

        let tag = sender.tag
        let data = arrDashboardVideosList[tag]
        
        if data.wishlist_count == "0"{
            data.wishlist_count = "1"
            self.addWishlistApi(requestID: data.id)
            let messageVC = UIAlertController(title: "", message: "Moved to holding area" , preferredStyle: .actionSheet)
            present(messageVC, animated: true) {
                            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (_) in
                                messageVC.dismiss(animated: true, completion: nil)})}
        }else{
            data.wishlist_count = "0"
            self.removeWishlistApi(requestID: data.id)
            let messageVC = UIAlertController(title: "", message: "Remove from holding area" , preferredStyle: .actionSheet)
            present(messageVC, animated: true) {
                            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (_) in
                                messageVC.dismiss(animated: true, completion: nil)})}
        }
        
        self.tblViewDashBoard.reloadData()
    }
    
    @IBAction func btnShareAction(_ sender: UIButton) {

        // Setting description
   //      let secondActivityItem = "com.Lazor://main"

         // Setting url
     //    let firstActivityItem : NSURL = NSURL(string: "com.Lazor://main")!
         
         // If you want to use an image
       //  let image : UIImage = UIImage(named: "Group 3645")!
     //    let activityViewController : UIActivityViewController = UIActivityViewController(
     //        activityItems: [ firstActivityItem, secondActivityItem, image], applicationActivities: nil)
        
        let someText:String = "com.Lazor://main"
        let objectsToShare:URL = URL(string: "com.Lazor://main")!
        let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
        let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
         
         // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = (sender )
         
         // This line remove the arrow of the popover to show in iPad
         activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
         activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
         
         // Pre-configuring activity items
        if #available(iOS 13.0, *) {
            activityViewController.activityItemsConfiguration = [
                UIActivity.ActivityType.message
            ] as? UIActivityItemsConfigurationReading
        } else {
            // Fallback on earlier versions
        }
         
         // Anything you want to exclude
         activityViewController.excludedActivityTypes = [
             UIActivity.ActivityType.postToWeibo,
             UIActivity.ActivityType.print,
             UIActivity.ActivityType.assignToContact,
             UIActivity.ActivityType.saveToCameraRoll,
             UIActivity.ActivityType.addToReadingList,
             UIActivity.ActivityType.postToFlickr,
             UIActivity.ActivityType.postToVimeo,
             UIActivity.ActivityType.postToTencentWeibo,
             UIActivity.ActivityType.postToFacebook
         ]
         
        if #available(iOS 13.0, *) {
            activityViewController.isModalInPresentation = true
        } else {
            // Fallback on earlier versions
        }
         self.present(activityViewController, animated: true, completion: nil)
        
        
    }
    
    @IBAction func btnUserProfileAction(_ sender: UIButton) {

        self.viewVideoPlayer.player?.pause()
        self.viewVideoPlayer.player = nil
        let tag = sender.tag
        let storyboard = staticClass.getStoryboard_Profile()
        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileNewViewController") as! ProfileNewViewController
        vc.strUserId = arrDashboardVideosList[tag].user[0].id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnCommentsAction(_ sender: UIButton) {
        let tag = sender.tag
        getCommentListApi(tag: tag)
    }
    
    
    //MARK: - API Methods
    func getDashboardVideosApi()  {
        SVProgressHUD.show()
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_DashboardList! as String + "\(currentPage)" as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    if let arrTemp = result.value(forKey: "data") as? NSArray{
                       // self.arrDashboardVideosList.removeAll()
                        for dict in arrTemp {
                            let data = DashboardVideosData.init(data: dict as! [String : Any])
                            self.arrDashboardVideosList.append(data)
                            
                        }
                        
//                            self.privateList = self.arrDashboardVideosList
//                            self.fromIndex =  self.arrDashboardVideosList.count
                          
                        self.isLoadingList = false
                        self.tblViewDashBoard.reloadData()
                     
                    }
                
            
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    
    func getCommentListApi(tag: Int)  {
        SVProgressHUD.show()
        
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_CommentsList! as String + "/" + arrDashboardVideosList[tag].id as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    if let arrTemp = result.value(forKey: "data") as? NSArray{
                        self.arrCommentList.removeAll()
                        for dict in arrTemp {
                            let data = CommentsBaseData.init(data: dict as! [String : Any])
                            self.arrCommentList.append(data)
                        }
                    }
                
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CommentsViewController") as! CommentsViewController
                    vc.arrCommentList = self.arrCommentList
                    vc.postId = self.arrDashboardVideosList[tag].id
                    self.present(vc, animated: true, completion: nil)

                   
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    func addWishlistApi(requestID: String)  {
        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["donation_request_id": requestID];
        
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_AddWishlist! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                    
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    func removeWishlistApi(requestID: String)  {
        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["donation_request_id": requestID];
        
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_RemoveWishlist! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                    
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
            }
            SVProgressHUD.dismiss()
        }
        
    }

    
}



