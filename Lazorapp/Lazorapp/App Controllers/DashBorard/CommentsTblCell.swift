//
//  CommentsTblCell.swift
//  Lazorapp
//
//  Created by SPDY on 31/05/21.
//

import UIKit
import ImageIO
import SwiftyGif

class CommentsTblCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblViewCommentsRiply: MyOwnTableView!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    
    @IBOutlet weak var btnRiply: UIButton!
    
    @IBOutlet weak var imgGifRiply: UIImageView!
    @IBOutlet weak var layoutGifRiplyHeight: NSLayoutConstraint!
    
    
    var arrCommentsRiply = [RepliesData]()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK:- UITableView Delegats
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        self.layoutRiplyTblHeight.constant = tblViewCommentsRiply.contentSize.height
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrCommentsRiply.count != 0{
            return arrCommentsRiply.count
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblViewCommentsRiply.dequeueReusableCell(withIdentifier: "CommentsRiplyTblCell") as! CommentsRiplyTblCell
        
        let data = arrCommentsRiply[indexPath.row]
        
        cell.lblName.text = data.replies_user[0].name
        let imgURL = URL(string: data.replies_user[0].image)
        cell.imgUser.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Group 3645"))
        
        if data.comment_type == "text"{
            cell.imgGifRiply.isHidden = true
            cell.lblComments.isHidden = false
            cell.lblComments.text = data.comment
            cell.layoutGifRiplyHeight.constant = 20
        }else{
            cell.imgGifRiply.isHidden = false
            cell.lblComments.isHidden = true
            cell.layoutGifRiplyHeight.constant = 60
            let gifURL : String = data.comment
            cell.imgGifRiply.image = UIImage.gifImageWithURL(gifURL)
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class CommentsRiplyTblCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var btnRiply: UIButton!
    
    @IBOutlet weak var imgGifRiply: UIImageView!
    @IBOutlet weak var layoutGifRiplyHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
