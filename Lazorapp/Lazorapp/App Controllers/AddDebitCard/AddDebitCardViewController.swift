//
//  AddDebitCardViewController.swift
//  Lazorapp
//
//  Created by SPDY on 31/05/21.
//

import UIKit
import SVProgressHUD

class AddDebitCardViewController: BaseViewController {

    
    @IBOutlet weak var viewCardNum: UIView!
    @IBOutlet weak var viewCVV: UIView!
    @IBOutlet weak var viewExpiryDate: UIView!
    
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtCVV: UITextField!
    
    @IBOutlet weak var txtExpDate: UITextField!
    @IBOutlet weak var txtExpYear: UITextField!
    
    var strExpiryDate = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewCardNum.addGrayViewBorder()
        viewExpiryDate.addGrayViewBorder()
        viewCVV.addGrayViewBorder()
        
        
        txtCardNumber.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        txtExpDate.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        txtExpYear.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        txtCVV.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
    }
    
    
    //When changed value in textField
    @objc func textFieldDidChange(textField: UITextField){
       
        if textField == txtCardNumber{
            
            if  txtCardNumber.text?.count == 16 {
                txtExpDate.becomeFirstResponder()
            }else if  txtCardNumber.text!.count < 16 {
                txtCardNumber.becomeFirstResponder()
            }
        }

        if textField == txtExpDate{
            if txtExpDate.text?.count == 2{
                txtExpDate.resignFirstResponder()
                txtExpYear.becomeFirstResponder()
            }
        }
        
        if textField  == txtExpYear{
            if txtExpYear.text?.count == 4{
                txtExpYear.resignFirstResponder()
                txtCVV.becomeFirstResponder()
            }
        }
        
        if textField == txtCVV{
            if txtCVV.text?.count == 3{
                txtCVV.resignFirstResponder()
            }
        }
       
        
       
    }
    
    //MARK:- UIButtons Action
    
    @IBAction func btnSaveAction(_ sender: Any) {
        
        if txtCardNumber.text?.isEmpty == true{
            self.showErrorMessage(msgCardNumber);
        }else if txtCardNumber.text!.count < 16{
            self.showErrorMessage(msgValidCardNumber);
        }else if txtExpDate.text?.isEmpty == true{
            self.showErrorMessage(msgExpDate);
        }else if txtExpYear.text?.isEmpty == true{
            self.showErrorMessage(msgExpYear);
        }else if txtCVV.text?.isEmpty == true{
            self.showErrorMessage(msgCVV);
        }else{
            strExpiryDate = txtExpDate.text! + "/" +  txtExpYear.text!
            print(strExpiryDate)
            addDebitCardApi()
        }
        
    }
    
    
    //MARK: - API Methods
    func addDebitCardApi()  {
        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["card_number": txtCardNumber.text!, "expiry_date": strExpiryDate, "cvv": txtCVV.text!];
        print(dictParam as Any)
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_CardDetails! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "IAMViewController") as! IAMViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }

    
}
