//
//  VideoSplashViewController.swift
//  Lazorapp
//
//  Created by SPDY on 29/05/21.
//

import UIKit
import Foundation
import AVKit
import SVProgressHUD
import SDWebImage

class VideoSplashViewController: BaseViewController,AVPlayerViewControllerDelegate {
    
    var videoURL = String()
    
    @IBOutlet weak var videoPlayer: UIView!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
        getSplashVideoApi()
        
    }
    
    
    //MARK:- UIButtons Action
    
    @IBAction func btnPlayAction(_ sender: Any) {
      
        let videoURL = URL(string: self.videoURL)
        let player = AVPlayer(url: videoURL! as URL)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = videoPlayer.bounds
        videoPlayer.layer.addSublayer(playerLayer)
        self.imgVideo.isHidden = true
        self.btnPlay.isHidden = true
        player.play()
        
    }
    
    
    @IBAction func btnSkipAction(_ sender: Any) {
        
        let strMain = UIStoryboard(name: "Main", bundle: nil)
        let vc = strMain.instantiateViewController(withIdentifier: "DashboardNavigation")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = vc
        
    }
    
    //MARK: - API Methods
    func getSplashVideoApi()  {
        SVProgressHUD.show()
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_SplashVideo! as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    let data = result.value(forKey: "data") as? NSDictionary
                    let urlThumbnail = data?["thumbnail"] as! String
                    print(urlThumbnail)
                    let imgURL = URL(string: urlThumbnail)
                    
                    let url = data?["onboarding_video"] as! String
                    print(url)
                    self.imgVideo.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Group 3645"))
                    self.videoURL = url
                   
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    
}
