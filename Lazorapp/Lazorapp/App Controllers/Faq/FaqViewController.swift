//
//  FaqViewController.swift
//  Lazorapp
//
//  Created by SPDY on 31/05/21.
//

import UIKit

class FaqViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblViewFaq: UITableView!
    let arrQuestions = ["Q1. Fusce vitae sapien ut turpis?","Q2. Aenean dapibus nulla sed eleifend?","Q3. Vestibulum sed libero venenatis?","Q4. Aenean dapibus nulla sed eleifend?","Q5. Vestibulum sed libero venenatis?"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQuestions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblViewFaq.dequeueReusableCell(withIdentifier: "FaqTblCell") as! FaqTblCell
        cell.lblQuestion.text = arrQuestions[indexPath.row]
        
        return cell
    }
    

}
