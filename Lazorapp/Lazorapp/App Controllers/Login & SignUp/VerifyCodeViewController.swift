//
//  VerifyCodeViewController.swift
//  Lazorapp
//
//  Created by SPDY on 30/05/21.
//

import UIKit
import SVProgressHUD

class VerifyCodeViewController: BaseViewController,UITextFieldDelegate {

    
    @IBOutlet weak var viewMainOTP: UIView!
    var otpText = String()
    var phoneNumber = String()
    var token = String()
    var strController = String()
    
    
    @IBOutlet weak var txtFirst: UITextField!
    @IBOutlet weak var txtSecond: UITextField!
    @IBOutlet weak var txtThird: UITextField!
    @IBOutlet weak var txtForth: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        showAlertController(sender: self, message: "OTP:- 1122", title: "")
        viewMainOTP.addGrayViewBorder()
        
        if #available(iOS 12.0, *) {
               txtFirst.textContentType = .oneTimeCode
               txtSecond.textContentType = .oneTimeCode
               txtThird.textContentType = .oneTimeCode
               txtForth.textContentType = .oneTimeCode
            
           }

           txtFirst.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
           txtSecond.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
           txtThird.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
            txtForth.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
           
           txtFirst.becomeFirstResponder() //by doing this it will open the keyboard on first text field automatically

    }
    
    
    func showAlertController(sender : Any, message : String, title : String) -> Void{
        
        let alert = UIAlertController(title: title as String,
                                      message: message as String,
                                      preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Okay", style:.default) {
              UIAlertAction in
              NSLog("OK Pressed")
          }
       
        alert.addAction(okAction)
       
        UIApplication.shared.keyWindow?.rootViewController!.present(alert, animated: true,completion: nil)
    
    }
    
    //When changed value in textField
       @objc func textFieldDidChange(textField: UITextField){
           let text = textField.text
        
//        if textField == txtFirst{
//            if txtFirst.text?.count == 1{
//                txtSecond.becomeFirstResponder()
//            }
//        }else if textField == txtSecond{
//            if txtSecond.text?.count == 1{
//                txtThird.becomeFirstResponder()
//            }
//        }else if textField == txtThird{
//            if txtThird.text?.count == 1{
//                txtForth.becomeFirstResponder()
//            }
//        }else if textField == txtForth{
//            if txtForth.text?.count == 1{
//                txtForth.resignFirstResponder()
//            }
//        }
        
        if  text!.count >= 1 {
               switch textField{

               case txtFirst:
                   txtSecond.becomeFirstResponder()
               case txtSecond:
                   txtThird.becomeFirstResponder()
               case txtThird:
                   txtForth.becomeFirstResponder()
               case txtForth:
                   self.dismissKeyboard()
               default:
                   break
               }
           }
           if  text?.count == 0 {
               switch textField{
               case txtFirst:
                   txtFirst.becomeFirstResponder()
               case txtSecond:
                   txtFirst.becomeFirstResponder()
               case txtThird:
                   txtSecond.becomeFirstResponder()
               case txtForth:
                   txtThird.becomeFirstResponder()
               default:
                   break
               }
           }
           
    }
    
    
    //MARK:- UITextFields Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
      
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
      
            let newLength: Int = (textField.text?.count)! + (string.count ) - range.length
            let csTxt = CharacterSet(charactersIn: ACCEPTABLE_MOBILE_NUMBER).inverted
            let filteredUsrTxt: String = string.components(separatedBy: csTxt).joined(separator: "")
            if (string == filteredUsrTxt) {
                if newLength > 1 {
                    return false
                }
            }
            else {
                return false
            }
            let length: Int = HelperClass.getLength(mobileNumber: textField.text!)
            //NSLog(@"Length  =  %d ",length);
            if length >= 1 {
                if range.length == 0 {
                    return false
                }
            }
        
        return true
    }
    

    
    func dismissKeyboard(){

           self.otpText = "\(self.txtFirst.text ?? "")\(self.txtSecond.text ?? "")\(self.txtThird.text ?? "")\(self.txtForth.text ?? "")"

           print(self.otpText)
           self.view.endEditing(true)

    }
    
    //MARK:- UIButtons Action
    @IBAction func btnVerifyAction(_ sender: Any) {
        
        if otpText.isEmpty == true{
            self.showErrorMessage(msgOTP);
        }else{
            if strController == "Forgot"{
                verifyOtpForgotPasswordApi()
            }else{
                verifyOtpApi()
            }
           
        }

    }
    
    
    //MARK: - API Methods
    func verifyOtpApi()  {
        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["phone": phoneNumber,"token": token, "otp": "\(otpText)"];
        print(dictParam!);
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : String], serverUrl: URL_VerifyOTP! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    let dict =  result.value(forKey: "data")as! NSDictionary
                    UserDefaults.standard.set(dict["access_token"], forKey: kAccessToken)
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpCreateProfileVC") as! SignUpCreateProfileVC
                    self.navigationController?.pushViewController(vc, animated: true)
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }

    func verifyOtpForgotPasswordApi()  {
        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["phone": phoneNumber,"token": token, "otp": "\(otpText)"];
        print(dictParam!);
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : String], serverUrl: URL_VerifyOTPForgotPassword! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                    let data = result.value(forKey: "data")as! NSDictionary
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
                    vc.token = data["token"] as! String;
                    self.navigationController?.pushViewController(vc, animated: true)
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
}
