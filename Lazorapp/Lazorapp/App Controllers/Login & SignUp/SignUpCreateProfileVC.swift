//
//  SignUpCreateProfileVC.swift
//  Lazorapp
//
//  Created by SPDY on 04/03/1943 Saka.
//

import UIKit
import Foundation
import SVProgressHUD
import Alamofire

class SignUpCreateProfileVC: BaseViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var imgUserProfile: UIImageView!
    
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtUniversity: UITextField!
    @IBOutlet weak var txtOccupation: UITextField!
    @IBOutlet weak var txtCityState: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtDOB: UITextField!
    
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewUsername: UIView!
    @IBOutlet weak var viewUniversity: UIView!
    @IBOutlet weak var viewOccupation: UIView!
    @IBOutlet weak var viewCityState: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewDOB: UIView!
    
    @IBOutlet weak var viewGradiant: UIView!
    @IBOutlet weak var btnShowPassword: UIButton!
    
    let datePicker = UIDatePicker()

    var imagePicker = UIImagePickerController();
    var imgBase64 = ""

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        showDatePicker()
        viewName.addGrayViewBorder()
        viewUsername.addGrayViewBorder()
        viewUniversity.addGrayViewBorder()
        viewOccupation.addGrayViewBorder()
        viewCityState.addGrayViewBorder()
        viewEmail.addGrayViewBorder()
        viewPassword.addGrayViewBorder()
        viewDOB.addGrayViewBorder()
        
        let gradient = CAGradientLayer()
        gradient.frame = viewGradiant.bounds
        let color1 = UIColor.clear.withAlphaComponent(0.8).cgColor
        gradient.colors = [(UIColor.clear.cgColor ),color1]
        
        viewGradiant.clipsToBounds = true
        viewGradiant.layer.cornerRadius = 20
        viewGradiant.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
     
        self.viewGradiant.layer.addSublayer(gradient)
    }
    
    //MARK:- Text Field Delegtae
    private func switchBasedNextTextField(_ textField: UITextField) {
        switch textField {
        case self.txtName:
            self.txtUsername.becomeFirstResponder()
        case self.txtUsername:
            self.txtCityState.becomeFirstResponder()
//        case self.txtUniversity:
//            self.txtOccupation.becomeFirstResponder()
//        case self.txtOccupation:
//            self.txtCityState.becomeFirstResponder()
        case self.txtCityState:
            self.txtEmail.becomeFirstResponder()
        case self.txtEmail:
            self.txtPassword.becomeFirstResponder()
        default:
            self.txtPassword.resignFirstResponder()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.switchBasedNextTextField(textField)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

      
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        return true
    }
    
    //MARK:- DatePicker
    @IBAction func datePickerChanged(_ sender: Any) {
        showDatePicker()
    }
    
    func showDatePicker(){
          //Formate Date
          datePicker.datePickerMode = .date
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }

        datePicker.set18YearValidation()
         //ToolBar
         let toolbar = UIToolbar();
         toolbar.sizeToFit()
         let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
         let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
         let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

       toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

        txtDOB.inputAccessoryView = toolbar
        txtDOB.inputView = datePicker

   }

    @objc func donedatePicker(){
        
         let formatter = DateFormatter()
         formatter.dateFormat = "yyyy/MM/dd"
         txtDOB.text = formatter.string(from: datePicker.date)
         self.view.endEditing(true)
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
        
    }
    

    //MARK:- UIButtons Action
    
    @IBAction func btnUpdateImageAction(_ sender: Any) {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallary", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func btnPasswordShowHide(_ sender: UIButton) {
        if sender.tag == 0{
            sender.tag = 1
            self.txtPassword.isSecureTextEntry = false
            if #available(iOS 13.0, *) {
                self.btnShowPassword.setImage(UIImage.init(systemName: "eye.slash"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
        }else{
            self.txtPassword.isSecureTextEntry = true
            if #available(iOS 13.0, *) {
                self.btnShowPassword.setImage(UIImage.init(systemName: "eye"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
            sender.tag = 0
        }
        
    }
    
    @IBAction func btnContinueAction(_ sender: Any) {
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "QuestionViewController") as! QuestionViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        
        if txtName.text?.isEmpty == true{
            self.showErrorMessage(msgName);
        }else if txtUsername.text?.isEmpty == true{
            self.showErrorMessage(msgUserName);
//        }else if txtUniversity.text?.isEmpty == true{
//            self.showErrorMessage(msgUniversity);
//        }else if txtOccupation.text?.isEmpty == true{
//            self.showErrorMessage(msgOccupation);
        }else if txtCityState.text?.isEmpty == true{
            self.showErrorMessage(msgAddress);
        }else if txtEmail.text?.isEmpty == true{
            self.showErrorMessage(msgEmail);
        }else if txtPassword.text?.isEmpty == true{
            self.showErrorMessage(msgPassword);
        }else if txtDOB.text?.isEmpty == true{
            self.showErrorMessage(msgDOB);
        }else{
           //createProfileApi();
            uploadUserDataApi()
        }
            
        
    }
    
    //MARK:- image Picker functions
    
      func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
              self.present(imagePicker, animated: true, completion: nil)
          }else{
            self.showErrorMessage(kCameraIssue)
             // HelperClass.showPopupAlertController(sender: self, message: kCameraIssue, title: kAlertTitle);

          }
      }
    
      func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
          self.present(imagePicker, animated: true, completion: nil)
      }
      
     //MARK:- UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        imgUserProfile.contentMode = .scaleToFill
        imgUserProfile.image = tempImage
        
        self.imgBase64 = ConvertImageToBase64String (img: tempImage)
        self.dismiss(animated: true, completion: nil)
    }
     
     func ConvertImageToBase64String (img: UIImage) -> String {
        let imageData:NSData = img.jpegData(compressionQuality: 0.50)! as NSData //UIImagePNGRepresentation(img)
         let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
         return imgString
     }

     func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
         print("picker cancel.")
         dismiss(animated: true, completion: nil)
     }
    
    
    //MARK: - API Methods
    func createProfileApi()  {
        SVProgressHUD.show()
        
        let dictParam : NSDictionary? =  ["name": txtName.text!, "email": txtEmail.text!, "username": txtUsername.text!, "password" : txtPassword.text!, "university": txtUniversity.text!, "occupation": txtOccupation.text!, "address": txtCityState.text!, "dob": txtDOB.text!];
        
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_CreateProfile! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                    
                    let dict =  result.value(forKey: "data")as! NSDictionary
                     UserDefaults.standard.set(dict, forKey: kUserData)
                   
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "QuestionViewController") as! QuestionViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    
    func uploadUserDataApi(){

        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["name": txtName.text!, "email": txtEmail.text!, "username": txtUsername.text!, "password" : txtPassword.text!, "university": txtUniversity.text ?? "", "occupation": txtOccupation.text!, "address": txtCityState.text!, "dob": txtDOB.text!];

        print(dictParam as Any)
        let imgDData = imgUserProfile.image!.jpegData(compressionQuality: 0.3)
        
        let accessToken = UserDefaults.standard.string(forKey: kAccessToken)
        let newAcess = accessToken ?? ""
        print(accessToken ?? "")
        
        let newToken = "Bearer " + String(describing: newAcess)
        print(newToken)

        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "cache-control": "no-cache",
            "Authorization": newToken
//                "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOTE4YWZlOTM0ZTJjMDIxOTEyZmU3ZWJhMGE0MWQ0YjkxZmI3ZDU2MjNiZTVkOWI5YWI3OTc2NjY4ODRmMTdkODc5NDJmMGQ1YTc1NDM4MGQiLCJpYXQiOjE2MjM4MzA1NjAuMzgxMjgsIm5iZiI6MTYyMzgzMDU2MC4zODEzMSwiZXhwIjoxNjU1MzY2NTYwLjMwNDIxNywic3ViIjoiMSIsInNjb3BlcyI6W119.Sc0n9xy3jzNztX4Uqle5KR936WawAz2A9H4F8r3CuEXDXgRG8Yg7uh6xlig-n1cxKkoIcm5fqCQ4Dz-V2qvDl9uMPQYS_kV14kVAg8vnkZ_JkZO0zsLG6wNMetuGltFrSsizKcAGOeOFU4BoIxV3RudhQhrh1Yl0YU_zMShAHr2B-R7NElozKBzJhy9PEtWV7dU8LpUy4lFuV6CK3OTJ8jjseUxf7osQgWUf8VlmYG3uGm3oghM2j1355SkEHKaybM_EIiiVYR1uFX2SNWt6D-yhCpIWFKN2zi57e4eeFDLYIM64PEGtZkv_osQaFI6alTdEFqv1j7jk4DFOuMBSV9CjAtjGOqwLJXB04bcdoJIZwOfrz-aMDvtIeyCRMbQtfSf6-VF7lJ6R2-SqXJVjAu_Tybph2A_QFO57N9jEmrW980Iw4noW5_TdVPx0vwZcvbXmDZQHHWY0uh2sCzOqUxHUDEA-OYT2B6l3ZfgJQnkLcqLHT7uOAHWjUu2x3-W-H0Vor34q-sftii_upAmBb_YWGWtemvLGpbh50D0DZnFp1IF7haOefRZVt705uEvtcdAOD3Nk68kk3I16mG_0Gv2COqU8JaOiPnvjMdprsN7JsdDmZrZHnTA40B6urwGBdDbzyUrIJ9FiUxqsLCzCUP6FmneIOTBUxWK7NG3bfl8",
        ]
        
        let urlRequest = try! URLRequest(url: URL_CreateProfile! as String, method: .post, headers: headers)
           let multipartFormData = MultipartFormData();
           
           for (key, value) in dictParam! {
            multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!,withName: key as! String)
           }
        let imgData = imgDData
        print(imgData)
        multipartFormData.append(imgData!, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")

//        multipartFormData.append(comment as! Data, withName: "comment", fileName: "\(comment).gif", mimeType: "image/gif")
           AF.upload(multipartFormData: multipartFormData, with: urlRequest).responseJSON
           { (response) in

            print(response.result)

              do{
                  if let jsonData = response.data{
                      let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
                      print(parsedData)
                    
                    if (parsedData["statusCode"] as! NSInteger == 200){
                        
                        let dict =  parsedData["data"] as! NSDictionary
                         UserDefaults.standard.set(dict, forKey: kUserData)
                         UserDefaults.standard.set(dict["id"], forKey: kUserID)
            
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "QuestionViewController") as! QuestionViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                               
                     }else{
                        self.showErrorMessage(parsedData["message"] as! String)
                     
                    }
                      
                  }
              }catch{
                  print("error message")
              }
                
            SVProgressHUD.dismiss()
            

           }

    }
    
}



extension UIDatePicker {
    func set18YearValidation() {
        let currentDate: Date = Date()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(identifier: "UTC")!
        var components: DateComponents = DateComponents()
        components.calendar = calendar
        components.year = -18
        let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!
        components.year = -150
        let minDate: Date = calendar.date(byAdding: components, to: currentDate)!
        self.minimumDate = minDate
        self.maximumDate = maxDate
    }
    
}
