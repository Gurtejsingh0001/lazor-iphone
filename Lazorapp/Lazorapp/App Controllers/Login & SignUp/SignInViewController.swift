//
//  SignInViewController.swift
//  Lazorapp
//
//  Created by Rakesh Gupta on 5/19/21.
//

import UIKit
import SVProgressHUD

class SignInViewController: BaseViewController {

    
    @IBOutlet weak var viewUsername: UIView!
    @IBOutlet weak var viewPassword: UIView!
    
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var btnCheckBox: UIButton!
    
    @IBOutlet weak var btnShowPassword: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    
    var strControllerType = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if strControllerType == "Logout"{
            btnBack.isHidden = true
            btnSignUp.isHidden = false
        }else if strControllerType == "Forgot"{
            btnBack.isHidden = true
            btnSignUp.isHidden = false
        }else{
            btnBack.isHidden = false
            btnSignUp.isHidden = true
        }
        
//        txtUsername.text = "guruteg"
//        txtPassword.text = "00010001"

        txtPassword.clipsToBounds = true
        txtPassword.isSecureTextEntry = true
       
        viewUsername.addGrayViewBorder()
        viewPassword.addGrayViewBorder()
    }
    
    
    //Mark:- UIButtons Action
    @IBAction func btnCheckBoxAction(_ sender: UIButton) {
        if sender.tag == 0{
            btnCheckBox.setImage(UIImage.init(named: "check_ic"), for: .normal)
            sender.tag = 1
        }else{
            btnCheckBox.setImage(UIImage.init(named: "Uncheck_ic"), for: .normal)
            sender.tag = 0
        }
    }
    
    @IBAction func btnForgotPasswordAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC1") as! ForgotPasswordVC1
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnLoginAction(_ sender: Any) {
        
        if txtUsername.text?.isEmpty == true{
            self.showErrorMessage(msgUserName);
        }else if txtPassword.text?.isEmpty == true{
            self.showErrorMessage(msgPassword);
        }else{
            signInApi()
        }
        
    }
    
    
    @IBAction func btnPasswordShowHide(_ sender: UIButton) {
        if sender.tag == 0{
            sender.tag = 1
            self.txtPassword.isSecureTextEntry = false
            if #available(iOS 13.0, *) {
                self.btnShowPassword.setImage(UIImage.init(systemName: "eye.slash"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
        }else{
            self.txtPassword.isSecureTextEntry = true
            if #available(iOS 13.0, *) {
                self.btnShowPassword.setImage(UIImage.init(systemName: "eye"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
            sender.tag = 0
        }
        
    }
    
    //MARK: - API Methods
    func signInApi()  {
        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["username": txtUsername.text!, "password": txtPassword.text!];
        
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_LOGIN! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                    
                   let dict =  result.value(forKey: "data")as! NSDictionary
                    
                    UserDefaults.standard.set(dict["id"], forKey: kUserID)
                    UserDefaults.standard.set(dict, forKey: kUserData)
                    UserDefaults.standard.set(dict["access_token"], forKey: kAccessToken)
                  //  self.showErrorMessage(result.value(forKey: "message") as! String)
                    
//                    let messageVC = UIAlertController(title: "", message: result.value(forKey: "message") as! String , preferredStyle:  UIAlertController.Style.alert)
//                    self.present(messageVC, animated: true) {
//                                    Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false, block: { (_) in
//                                        messageVC.dismiss(animated: true, completion: nil)})}
                    
                     let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                    alertWindow.rootViewController = UIViewController()

                    let alertController = UIAlertController(title: kAlertTitle, message: result.value(forKey: "message") as! String, preferredStyle: UIAlertController.Style.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: { _ in
                        alertWindow.isHidden = true
                        let strMain = UIStoryboard(name: "Main", bundle: nil)
                        let vc = strMain.instantiateViewController(withIdentifier: "DashboardNavigation")
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = vc
                        
                    }))
                    
                    alertWindow.windowLevel = UIWindow.Level.alert + 1;
                    alertWindow.makeKeyAndVisible()
                    alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
                    
                    
//                    let strMain = UIStoryboard(name: "Main", bundle: nil)
//                    let vc = strMain.instantiateViewController(withIdentifier: "DashboardNavigation")
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    appDelegate.window?.rootViewController = vc
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }

    
}
