//
//  MyDonationsViewController.swift
//  Lazorapp
//
//  Created by SPDY on 28/05/21.
//

import UIKit
import SVProgressHUD

class MyDonationsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var tblViewMyDonations: UITableView!
    
    var arrDonationList = [DonationBaseData]()
    var strUserId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getDonationListApi()
    }
    

    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDonationList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblViewMyDonations.dequeueReusableCell(withIdentifier: "MyDonorsTblCell") as! MyDonorsTblCell
            let data = self.arrDonationList[indexPath.row]
            cell.lblAmount.text = data.donation_request[0].donation_amount
            cell.lblCategoryName.text = data.donation_request[0].category[0].name
            cell.lblTitle.text = data.donation_request[0].caption
            let imgURL = URL(string: data.donation_request[0].user[0].image)
            cell.imgView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Mask Group 84"))
        
        return cell
    }

    
    //MARK:- Api's Method
    func getDonationListApi()  {
        SVProgressHUD.show()
        
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_MyDonation! as String + strUserId + "&page=1" as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    if let arrTemp = result.value(forKey: "data") as? NSArray{
                        self.arrDonationList.removeAll()
                        for dict in arrTemp {
                            let data = DonationBaseData.init(data: dict as! [String : Any])
                            self.arrDonationList.append(data)
                        }
                    }
                
                    self.tblViewMyDonations.reloadData()
                    
                    if self.arrDonationList.count == 0{
                        self.tblViewMyDonations.isHidden = true
                    }
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }

}
