//
//  InviteViewController.swift
//  Lazorapp
//
//  Created by Apple on 28/07/21.
//

import UIKit

class InviteViewController: BaseViewController {

    @IBOutlet weak var viewReferCode: UIView!
    @IBOutlet weak var lblReferCode: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.async {
            self.viewReferCode.addDashedBorder(colorLightGray, withWidth: 3, cornerRadius: 10, dashPattern: [3,4])
        }
        
    }
    
    //MARK:- UIButtons Action
    @IBAction func btnInviteAction(_ sender: Any) {
       
        
    }
    

}
