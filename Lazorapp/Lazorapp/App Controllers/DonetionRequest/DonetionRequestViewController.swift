//
//  DonetionRequestViewController.swift
//  Lazorapp
//
//  Created by SPDY on 27/05/21.
//

import UIKit
import SVProgressHUD
import iOSDropDown
import Alamofire

class DonetionRequestViewController: BaseViewController, UITextViewDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var viewUploadVideo: UIView!
    @IBOutlet weak var viewSelectCategory: UIView!
    @IBOutlet weak var viewCaptionVideo: UIView!
    @IBOutlet weak var viewDonationAmount: UIView!
    
    @IBOutlet weak var btnCheckBox: UIButton!
    
    @IBOutlet var viewPopUp: UIView!
    @IBOutlet var viewBGPopUp: UIView!
    

    @IBOutlet weak var txtUploadVideo: UITextField!
    
    @IBOutlet weak var txtCategory: DropDown!
    @IBOutlet weak var txtViewCaptionVideo: UITextView!
    @IBOutlet weak var txtDonationAmount: UITextField!
    
    var arrCategoryString = [String]()
    
    
    let imagePickerController = UIImagePickerController()
    var videoURL: NSURL?
    var videoData: Data?
    var catId = String()
    
    var arrCategoryList = [DonationCategoryData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCategoryApi()
        // Initialize Tap Gesture Recognizer
           let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapView(_:)))
        viewBGPopUp.addGestureRecognizer(tapGestureRecognizer)
        DispatchQueue.main.async {
            self.viewUploadVideo.addDashedBorder(colorLightGray, withWidth: 3, cornerRadius: 10, dashPattern: [3,4])
        }
        
        viewSelectCategory.addGrayViewBorder()
        viewCaptionVideo.addGrayViewBorder()
        viewDonationAmount.addGrayViewBorder()
    }
    
    //MARK:- UITextView Delegets
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Caption to Video"{
            self.txtViewCaptionVideo.text = ""
            self.txtViewCaptionVideo.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            self.txtViewCaptionVideo.text = "Caption to Video"
            self.txtViewCaptionVideo.textColor = colorLightGray
        }
    }
    
    
    //MARK:- UIImagePickerControllerDelegate
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
      
    let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
       if let url = info[.mediaURL] as? URL {
           do {
             try FileManager.default.moveItem(at: url, to: documentsDirectoryURL.appendingPathComponent("videoName.mp4"))
   
               print("movie saved")
           } catch {
               print(error)
           }
        print(url)
       }
    
    
            videoURL = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.mediaURL.rawValue)]as? NSURL
            print(videoURL!)
    
//        let videoURL = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerReferenceURL")] as? NSURL
//        print(videoURL!)
    
    
        if let videoPathUrl = videoURL {
            do {
                let video3 = try NSData(contentsOf: videoPathUrl as URL, options: .mappedIfSafe)
                print(video3)
                
                
                videoData = try Data(contentsOf: (videoPathUrl as NSURL) as URL)
                print(videoData as Any)
            } catch {
                print("Unable to load data: \(error)")
            }
        }
    
        self.txtUploadVideo.text = String(describing: videoURL!)
    
    
        imagePickerController.dismiss(animated: true, completion: nil)
   }
    
    
    // MARK: - Actions

    @IBAction func selectImageFromPhotoLibrary(sender: UIBarButtonItem) {
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        imagePickerController.mediaTypes = ["public.image", "public.movie"]

        present(imagePickerController, animated: true, completion: nil)
    }

    
    @IBAction func didTapView(_ sender: UITapGestureRecognizer) {
     //   viewPopUp.removeFromSuperview()
    
    }
    
    func openVideoGallery() {
        
        imagePickerController.delegate = self
        imagePickerController.sourceType = .savedPhotosAlbum
        imagePickerController.mediaTypes = ["public.movie"]
        imagePickerController.allowsEditing = false
        present(imagePickerController, animated: true, completion: nil)
    }
    
    //MARK:- UIButtons Action
    @IBAction func btnUploadVideoAction(_ sender: Any) {
        
        
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        imagePickerController.mediaTypes = ["public.movie"]

        present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func btnOkayAction(_ sender: Any) {
        
        viewPopUp.removeFromSuperview()
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        if txtUploadVideo.text?.isEmpty == true{
            self.showErrorMessage(msgVideo);
        }else if txtCategory.text?.isEmpty == true{
            self.showErrorMessage(msgCategory);
        }else if txtViewCaptionVideo.text?.isEmpty == true{
            self.showErrorMessage(msgVideoCaption);
        }else if txtDonationAmount.text?.isEmpty == true{
            self.showErrorMessage(msgDonationAmount);
        }else{
          //  submitDonationRequestApi()
            uploadvideoData()
        }
        
        
        
    }
    
    @IBAction func btnCheckBoxAction(_ sender: UIButton) {
        if sender.tag == 0{
            btnCheckBox.setImage(UIImage.init(named: "check_ic"), for: .normal)
            sender.tag = 1
        }else{
            btnCheckBox.setImage(UIImage.init(named: "Uncheck_ic"), for: .normal)
            sender.tag = 0
        }

    }
    
    //MARK: - API Methods
    func getCategoryApi()  {
        SVProgressHUD.show()
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_DonationCategory! as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    if let arrTemp = result.value(forKey: "data") as? NSArray{
                        self.arrCategoryList.removeAll()
                        self.arrCategoryString.removeAll()
                        for dict in arrTemp {
                            let data = DonationCategoryData.init(data: dict as! [String : Any])
                            self.arrCategoryString.append(data.name)
                            self.arrCategoryList.append(data)
                        }
                    }
                    
                    self.txtCategory.optionArray = self.arrCategoryString
                    // The the Closure returns Selected Index and String
                    self.txtCategory.didSelect{(selectedText , index ,id) in
                        self.txtCategory.text = selectedText
                        
                            for i in self.arrCategoryList{
                                if self.txtCategory.text == i.name{
                                    print(i.id)
                                    self.catId = i.id
                                }
                            }
                        }
                
                   
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    
    func submitDonationRequestApi()  {
        SVProgressHUD.show()
        
        print(self.catId)
        let dictParam : NSDictionary? =  ["category_id": self.catId, "video": txtUploadVideo.text!, "caption": txtViewCaptionVideo.text!, "donation amount" : txtDonationAmount.text!];
        
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_CreateDonationRequest! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    self.viewPopUp.frame = self.view.bounds
                    self.view.addSubview(self.viewPopUp)
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    
    func uploadvideoData(){

        SVProgressHUD.show()
        print(self.catId)
        let dictParam : NSDictionary? =  ["category_id": self.catId, "caption": txtViewCaptionVideo.text!, "donation amount" : txtDonationAmount.text!];

        print(dictParam as Any)
        
        let accessToken = UserDefaults.standard.string(forKey: kAccessToken)
        let newAcess = accessToken ?? ""
        print(accessToken ?? "")
        
        let newToken = "Bearer " + String(describing: newAcess)
        print(newToken)

        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "cache-control": "no-cache",
            "Authorization": newToken
//                "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOTE4YWZlOTM0ZTJjMDIxOTEyZmU3ZWJhMGE0MWQ0YjkxZmI3ZDU2MjNiZTVkOWI5YWI3OTc2NjY4ODRmMTdkODc5NDJmMGQ1YTc1NDM4MGQiLCJpYXQiOjE2MjM4MzA1NjAuMzgxMjgsIm5iZiI6MTYyMzgzMDU2MC4zODEzMSwiZXhwIjoxNjU1MzY2NTYwLjMwNDIxNywic3ViIjoiMSIsInNjb3BlcyI6W119.Sc0n9xy3jzNztX4Uqle5KR936WawAz2A9H4F8r3CuEXDXgRG8Yg7uh6xlig-n1cxKkoIcm5fqCQ4Dz-V2qvDl9uMPQYS_kV14kVAg8vnkZ_JkZO0zsLG6wNMetuGltFrSsizKcAGOeOFU4BoIxV3RudhQhrh1Yl0YU_zMShAHr2B-R7NElozKBzJhy9PEtWV7dU8LpUy4lFuV6CK3OTJ8jjseUxf7osQgWUf8VlmYG3uGm3oghM2j1355SkEHKaybM_EIiiVYR1uFX2SNWt6D-yhCpIWFKN2zi57e4eeFDLYIM64PEGtZkv_osQaFI6alTdEFqv1j7jk4DFOuMBSV9CjAtjGOqwLJXB04bcdoJIZwOfrz-aMDvtIeyCRMbQtfSf6-VF7lJ6R2-SqXJVjAu_Tybph2A_QFO57N9jEmrW980Iw4noW5_TdVPx0vwZcvbXmDZQHHWY0uh2sCzOqUxHUDEA-OYT2B6l3ZfgJQnkLcqLHT7uOAHWjUu2x3-W-H0Vor34q-sftii_upAmBb_YWGWtemvLGpbh50D0DZnFp1IF7haOefRZVt705uEvtcdAOD3Nk68kk3I16mG_0Gv2COqU8JaOiPnvjMdprsN7JsdDmZrZHnTA40B6urwGBdDbzyUrIJ9FiUxqsLCzCUP6FmneIOTBUxWK7NG3bfl8",
        ]
        
        let urlRequest = try! URLRequest(url: URL_CreateDonationRequest! as String, method: .post, headers: headers)
           let multipartFormData = MultipartFormData();
           
           for (key, value) in dictParam! {
            multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!,withName: key as! String)
           }
    
        print(videoData as Any)
        multipartFormData.append(videoData!, withName: "video", fileName: "file.mp4", mimeType: "video/mp4")
        
//        multipartFormData.append(comment as! Data, withName: "comment", fileName: "\(comment).gif", mimeType: "image/gif")
           AF.upload(multipartFormData: multipartFormData, with: urlRequest).responseJSON
           { (response) in

            print(response as Any)
            
            self.txtCategory.text = ""
            self.txtUploadVideo.text = ""
            self.txtViewCaptionVideo.text = ""
            self.txtDonationAmount.text = ""
           
            switch response.result {
                  case .success:
                   
                    self.viewPopUp.frame = self.view.bounds
                    self.view.addSubview(self.viewPopUp)
                case .failure: break
                      // error handling
                  }
            
          
           // self.showErrorMessage(response.value(forKey: "message") as! String)
            SVProgressHUD.dismiss()
            

           }

    }
    
}

extension UIView {
  func addDashedBorder(_ color: UIColor = UIColor.black, withWidth width: CGFloat = 2, cornerRadius: CGFloat = 5, dashPattern: [NSNumber] = [3,6]) {

    let shapeLayer = CAShapeLayer()

    shapeLayer.bounds = bounds
    shapeLayer.position = CGPoint(x: bounds.width/2, y: bounds.height/2)
    shapeLayer.fillColor = nil
    shapeLayer.strokeColor = color.cgColor
    shapeLayer.lineWidth = width
    shapeLayer.lineJoin = CAShapeLayerLineJoin.round // Updated in swift 4.2
    shapeLayer.lineDashPattern = dashPattern
    shapeLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath

    self.layer.addSublayer(shapeLayer)
  }
}
