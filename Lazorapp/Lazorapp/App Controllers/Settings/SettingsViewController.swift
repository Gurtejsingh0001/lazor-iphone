//
//  SettingsViewController.swift
//  Lazorapp
//
//  Created by SPDY on 31/05/21.
//

import UIKit

class SettingsViewController: BaseViewController {

    
    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var switchDemo: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()

        switchDemo.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        viewMain.addGrayViewBorder()
    }
    
    //MARK:- UIButtons Action
    
    @IBAction func btnSettingsAction(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            let vc = storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 2:
            let vc = storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 3:
            let vc = storyboard?.instantiateViewController(withIdentifier: "FaqViewController") as! FaqViewController
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            self.showLogoutAlertPopup(sender: self, message: msgLogout, title: kAlertTitle)
        }
         
    }
    
    
    func showLogoutAlertPopup(sender : Any, message : String, title : String) -> Void{
        
        let alert = UIAlertController(title: title as String,
                                      message: message as String,
                                      preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style:.default) {
              UIAlertAction in
              NSLog("OK Pressed")
            UserDefaults.standard.removeObject(forKey: kUserData)
            let storyboard = staticClass.getStoryboard_PreLogin()
            let vc = storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
            vc.strControllerType = "Logout"
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
       
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel, handler: nil)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        UIApplication.shared.keyWindow?.rootViewController!.present(alert, animated: true,completion: nil)
    
    }

    

}
