//
//  MyHoldingAreaViewController.swift
//  Lazorapp
//
//  Created by SPDY on 27/05/21.
//

import UIKit
import SVProgressHUD
import iOSDropDown

class MyHoldingAreaViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource{
    
    
    @IBOutlet weak var viewDonationAmount: UIView!
    @IBOutlet weak var lblDonationAmount: UILabel!
    
    
    @IBOutlet weak var tblViewList: UITableView!
    @IBOutlet weak var btnBadges: UIButton!
    
    
    @IBOutlet var viewBadgesPopUp: UIView!
    
    var arrWishlist = [WishlistData]()
    var arrAmount = ["$100","$200","$300","$400","$500"]
    var intDonationAmount = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapView(_:)))
        viewBadgesPopUp.addGestureRecognizer(tapGestureRecognizer)
        
        viewDonationAmount.addGreenViewBorder()
        btnBadges.layer.cornerRadius = 10
        btnBadges.layer.borderColor = colorDarkGray.cgColor
        btnBadges.layer.borderWidth = 1
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getMyHoldingAreaApi()
    }
    
    @IBAction func didTapView(_ sender: UITapGestureRecognizer) {
        viewBadgesPopUp.removeFromSuperview()
    
    }
    
    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrWishlist.count != 0{
            return arrWishlist.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arrWishlist.count != 0{
            let cell = self.tblViewList.dequeueReusableCell(withIdentifier: "MyHoldingAreaTblCell") as! MyHoldingAreaTblCell
            cell.viewMain.addGrayViewBorder()
            let data = self.arrWishlist[indexPath.row]
            let imgURL = URL(string: data.thumbnail)
            cell.imgView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "Lazor"))
            cell.lblTitle.text = data.caption
            cell.lblViewers.text = data.views_count
            cell.lblAmountOf.text = "of $" + data.donation_amount
            cell.lblAmountRaised.text = "$" + data.donation_received + " raised"
            cell.txtSelectAmount.text = data.selectedDonationAmount
            
       //     let temp =  Int(data.donation_amount)! - Int(data.donation_received)!
           
        //    let percentProgress = Float(Float(temp)*100.0/10.0)
         //   cell.viewProgressBar.setProgress(percentProgress, animated: true)
            DispatchQueue.main.async {
                let max =  Int(data.donation_amount)!
                let min = Int(data.donation_received)!
                cell.viewProgressBar.setProgress(Float(max), animated: true)
                cell.viewProgressBar.progress = Float(min)
            }
            
           // cell.viewProgressBar.progress =
                
            cell.txtSelectAmount.optionArray = arrAmount
            // The the Closure returns Selected Index and String
            cell.txtSelectAmount.didSelect{(selectedText , index ,id) in
              //  cell.txtSelectAmount.text = selectedText
            data.selectedDonationAmount = selectedText
                
//            let str = data.selectedDonationAmount
//            let str2 = str.replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
//            print(str2 as Any)
//            let intstr2 = Int(str2)! as Int
//
//            self.intDonationAmount
//                += intstr2
//
//            let str3 =  "$" + "\(self.intDonationAmount)"
//            self.lblDonationAmount.text = str3
                self.intDonationAmount = 0
                for i in self.arrWishlist{
                    let str = i.selectedDonationAmount
                    let str2 = str.replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
                    print(str2 as Any)
                    let intstr2 = (Int(str2) ?? 0) as Int
                    
                    self.intDonationAmount
                        += intstr2
                        
                    let str3 =  "$" + "\(self.intDonationAmount)"
                    self.lblDonationAmount.text = str3
                }
                
            }
            
            return cell
        }
        let cell = self.tblViewList.dequeueReusableCell(withIdentifier: "NoDataTblCell") as! MyHoldingAreaTblCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrWishlist.count != 0{
//            let vc = storyboard?.instantiateViewController(withIdentifier: "CheckoutViewController") as! CheckoutViewController
//            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    
    
    //MARK:- UIButtons Action
    @IBAction func btnDonationNowAction(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "CheckoutViewController") as! CheckoutViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btnBadgesAction(_ sender: Any) {
        
        self.viewBadgesPopUp.frame = self.view.bounds
        self.view.addSubview(self.viewBadgesPopUp)
        
    }
    
    
    //MARK: - API Methods
    func getMyHoldingAreaApi()  {
        SVProgressHUD.show()
        HelperClass.requestForAllApiyWithBody(serverUrl: URL_Wishlist! as String)
        { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    if let arrTemp = result.value(forKey: "data") as? NSArray{
                        self.arrWishlist.removeAll()
                        for dict in arrTemp {
                            let data = WishlistData.init(data: dict as! [String : Any])
                            self.arrWishlist.append(data)
                        }
                        self.tblViewList.reloadData()
                    }
                   
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }

}

