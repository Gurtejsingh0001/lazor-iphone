//
//  MyHoldingAreaTblCell.swift
//  Lazorapp
//
//  Created by SPDY on 27/05/21.
//

import UIKit
import iOSDropDown

class MyHoldingAreaTblCell: UITableViewCell {

    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblViewers: UILabel!
    @IBOutlet weak var viewMainSelectAmount: UIView!
    @IBOutlet weak var lblAmountRaised: UILabel!
    @IBOutlet weak var lblAmountOf: UILabel!
    @IBOutlet weak var txtSelectAmount: DropDown!
    @IBOutlet weak var viewProgressBar: UIProgressView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
