//
//  CashOutWithdrawViewController.swift
//  Lazorapp
//
//  Created by SPDY on 28/05/21.
//

import UIKit

class CashOutWithdrawViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblCashOutWithdraw: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblCashOutWithdraw.dequeueReusableCell(withIdentifier: "CashOutWithdrawTblCell") as! CashOutWithdrawTblCell
        return cell
    }
    
    
    //MARK:- UIButtons Action
    @IBAction func btnContinueAction(_ sender: Any) {
        
        
    }
    


}
