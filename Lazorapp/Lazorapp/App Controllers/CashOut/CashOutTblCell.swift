//
//  CashOutTblCell.swift
//  Lazorapp
//
//  Created by SPDY on 27/05/21.
//

import UIKit

class CashOutTblCell: UITableViewCell {

    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblReceivedAmount: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var btnCheck: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewMain.addGrayViewBorder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


class CashOutWithdrawTblCell: UITableViewCell {

    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewMain.addGrayViewBorder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
