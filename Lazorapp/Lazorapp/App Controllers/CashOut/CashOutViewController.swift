//
//  CashOutViewController.swift
//  Lazorapp
//
//  Created by SPDY on 27/05/21.
//

import UIKit

class CashOutViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblViewCashOut: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblViewCashOut.dequeueReusableCell(withIdentifier: "CashOutTblCell") as! CashOutTblCell
        if indexPath.row == 1{
            cell.btnCheck.isHidden = false
        }else if indexPath.row == 3{
            cell.btnCheck.isHidden = false
        }else{
            cell.btnCheck.isHidden = true
        }
        return cell
    }
    
    
    //MARK:- UIButtons Action
    @IBAction func btnContinueAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CashOutWithdrawViewController") as! CashOutWithdrawViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnAllAction(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "CashOutWithdrawViewController") as! CashOutWithdrawViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}
