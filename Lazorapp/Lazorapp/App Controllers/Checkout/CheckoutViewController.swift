//
//  CheckoutViewController.swift
//  Lazorapp
//
//  Created by SPDY on 27/05/21.
//

import UIKit

class CheckoutViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    

    
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblProcessingFee: UILabel!
    @IBOutlet weak var lblSubscriptionFee: UILabel!
    
    
    @IBOutlet weak var tblViewCheckout: UITableView!
    
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var viewUploadVideo: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.async {
            self.viewUploadVideo.addDashedBorder(colorLightGray, withWidth: 3, cornerRadius: 10, dashPattern: [3,4])
        }
    }
    
    
    //MARK:- UITableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblViewCheckout.dequeueReusableCell(withIdentifier: "CheckoutTblCell") as! CheckoutTblCell
        return cell
    }
    
    
    //MARK:- UIButtons Action
    @IBAction func btnContinuePayAction(_ sender: Any) {
        
        
    }
    
    @IBAction func btnCheckBoxAction(_ sender: UIButton) {
        if sender.tag == 0{
            btnCheckBox.setImage(UIImage.init(named: "check_ic"), for: .normal)
            sender.tag = 1
        }else{
            btnCheckBox.setImage(UIImage.init(named: "Uncheck_ic"), for: .normal)
            sender.tag = 0
        }
        
    }
    
    @IBAction func btnUploadVideoAction(_ sender: Any) {
        
    }
    
}
