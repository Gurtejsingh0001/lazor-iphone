//
//  CheckoutTblCell.swift
//  Lazorapp
//
//  Created by SPDY on 27/05/21.
//

import UIKit

class CheckoutTblCell: UITableViewCell {

    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        viewMain.addGrayViewBorder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
