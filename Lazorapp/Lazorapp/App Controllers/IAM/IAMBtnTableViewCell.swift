//
//  IAMBtnTableViewCell.swift
//  Lazorapp
//
//  Created by SPDY on 04/03/1943 Saka.
//

import UIKit

class IAMBtnTableViewCell: UITableViewCell {

    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewInner: UIView!
    
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var lblBtnTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
