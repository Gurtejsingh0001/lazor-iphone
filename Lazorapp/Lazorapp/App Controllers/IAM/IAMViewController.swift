//
//  IAMViewController.swift
//  Lazorapp
//
//  Created by SPDY on 04/03/1943 Saka.
//

import UIKit
import SVProgressHUD

class IAMViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {


    @IBOutlet weak var tblBtnsList: UITableView!
    let arrBtnString = ["Interested in being a donor and a recipient", "I’m just here to donate", "I’m unsure"]
    var strIAMRole = String()
    var tagButton = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    
    //MARK:- TableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrBtnString.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblBtnsList.dequeueReusableCell(withIdentifier: "IAMBtnTableViewCell") as! IAMBtnTableViewCell
        cell.lblBtnTitle.text = arrBtnString[indexPath.row]
        if indexPath.row == tagButton{
            cell.imgCheck.image = UIImage.init(named: "checkedWhite")
            cell.viewInner.isHidden = false
            cell.viewMain.layer.backgroundColor = colorGreen.cgColor
            cell.lblBtnTitle.textColor = UIColor.white
            cell.viewMain.layer.borderWidth = 0
        }else{
            cell.imgCheck.image = UIImage.init(named: "checkedGray")
            cell.viewInner.isHidden = true
            cell.viewMain.layer.backgroundColor = UIColor.white.cgColor
            cell.lblBtnTitle.textColor = UIColor.black
            cell.viewMain.layer.borderWidth = 1
            cell.viewMain.layer.borderColor = UIColor.lightGray.cgColor
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tagButton = indexPath.row
        if indexPath.row == 0{
            strIAMRole = "donor"
        }else if indexPath.row == 1{
            strIAMRole = "recipient"
        }else{
            strIAMRole = "both"
        }
        iAMApi()
        self.tblBtnsList.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    
//    @IBAction func btnAction(sender: UIButton){
//        tagButton = sender.tag
//    }
    
    
    //MARK: - API Methods
    func iAMApi()  {
        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["role": strIAMRole];
        
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : Any], serverUrl: URL_IAM! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TellMeYourselfVC") as! TellMeYourselfVC
//                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    let storyboard = staticClass.getStoryboard_VideoSplash()
                    let vc = storyboard?.instantiateViewController(withIdentifier: "VideoSplashViewController") as! VideoSplashViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }


}
