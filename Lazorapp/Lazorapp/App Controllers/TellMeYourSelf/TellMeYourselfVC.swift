//
//  TellMeYourselfVC.swift
//  Lazorapp
//
//  Created by SPDY on 27/05/21.
//

import UIKit
import SVProgressHUD

class TellMeYourselfVC: BaseViewController {

    @IBOutlet weak var txtView: UITextView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtView.layer.cornerRadius = 10
        txtView.layer.borderColor = colorLightGray.cgColor
        txtView.layer.borderWidth = 1
        
    }

    //MARK:- UIButtons Actions
    @IBAction func btnContinueAction(_ sender: Any) {
        
        if txtView.text.isEmpty == true{
            self.showErrorMessage(msgAboutProfile);
        }else{
            aboutYourselfApi()
        }
        
    }
    
    //MARK: - API Methods
    func aboutYourselfApi()  {
        SVProgressHUD.show()
        let dictParam : NSDictionary? =  ["about": txtView.text!];
        print(dictParam!);
        HelperClass.requestForAllApiyWithBody(param: dictParam! as! [String : String], serverUrl: URL_AboutProfile! as String, andHeader: false) { (result, error, success) in
            if success{
                if (result.value(forKey: "statusCode")as! NSInteger == 200){
                   
                    let storyboard = staticClass.getStoryboard_VideoSplash()
                    let vc = storyboard?.instantiateViewController(withIdentifier: "VideoSplashViewController") as! VideoSplashViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                           
                 }else{
                    self.showErrorMessage(result.value(forKey: "message") as! String)
                 
                }
            }else if error != nil {
                self.showErrorMessage((error?.localizedDescription)!)
              
            }else {
                self.showErrorMessage("Request failed,Please try again")
               // HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss()
        }
        
    }
    
    
}
