//
//  AllApiModelData.swift
//  Lazorapp
//
//  Created by SPDY on 30/06/21.
//

import Foundation

class AllApiModalData: NSObject {

}



class UserDHolder: NSObject {
    
    private static var singltonVar:UserDHolder?
    
    @objc var id                = ""
    @objc var phone             = ""
    @objc var occupation        = ""
    @objc var created_at        = ""
    @objc var dob               = ""
    @objc var about             = ""
    @objc var screen            = ""
    @objc var image             = ""
    @objc var university        = ""
    @objc var address           = ""
    @objc var username          = ""
    @objc var role              = ""
    @objc var updated_at        = ""
    @objc var email             = ""
    @objc var name              = ""
    @objc var country_code      = ""
    
    
   
    class var sharedInstance : UserDHolder
    {
        guard let uwshared  = singltonVar else {
            singltonVar = UserDHolder()
            return singltonVar!
        }
        return uwshared
        
    }
    
    class func destroy() {

        singltonVar = nil
        
    }
    
}

class SequrityQuestions: NSObject {
    
    @objc var id = ""
    @objc var updated_at = ""
    @objc var question = ""
    @objc var created_at = ""
    @objc var answer = "Answer"
    

    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            self.setValue(value, forKey: key)
        }
    }
    
    override init() {
        super.init()
    }
    
}


class DashboardVideosData: NSObject {
    
    @objc var id = ""
    @objc var user_id = ""
    @objc var category_id = ""
    @objc var video = ""
    @objc var thumbnail = ""
    @objc var caption = ""
    @objc var donation_amount = ""
    @objc var is_prime = ""
    @objc var status = ""
    @objc var created_at = ""
    @objc var updated_at = ""
    @objc var views_count = ""
    @objc var comments_count = ""
    @objc var shares_count = ""
    @objc var wishlist_count = ""
    @objc var donation_received = ""
    @objc var user = [UserDashboardData]()
    @objc var category = [CategoryDashboardData]()
    
    
    init(data: Dictionary<String,Any>) {
        super.init()
        
        for (key,value) in data {
            if let v = value as? Dictionary<String,Any> {
                
                if key == "user" {
                       var arr = [UserDashboardData]()
                     //  for dataModal in v {
                           let modal = UserDashboardData(data: v )
                           arr.append(modal)
                      // }
                       self.setValue(arr, forKey: key)
                }else{
                        var arr = [CategoryDashboardData]()
                      //  for dataModal in v {
                            let modal = CategoryDashboardData(data: v)
                            arr.append(modal)
                      //  }
                        self.setValue(arr, forKey: key)
                }
                
              //  if key == "user" {
//                    for (key,value) in v {
//                        self.setValue(value, forKey: key)
//                    }
//                    var arr = [UserDashboardData]()
//                    for dataModal in v {
//                        let modal = UserDashboardData(data: dataModal )
//                        arr.append(modal)
//                    }
//                    self.setValue(arr, forKey: key)
//                }else{
//                    var dict = CategoryDashboardData()
//                    for (key,value) in dict {
//                        self.setValue(value, forKey: key)
//                    }
//                }
                
            }else{
                self.setValue(value, forKey: key)
            }
            
        }
        
    }
    
    override init() {
        super.init()
    }
    
}

class UserDashboardData: NSObject {
    
    @objc var id = ""
    @objc var name = ""
    @objc var notification = ""
    @objc var status = ""
    @objc var email = ""
    @objc var country_code = ""
    @objc var phone = ""
    @objc var image = ""
    @objc var username = ""
    @objc var university = ""
    @objc var occupation = ""
    @objc var address = ""
    @objc var dob = ""
    @objc var role = ""
    @objc var about = ""
    @objc var screen = ""
    @objc var created_at = ""
    @objc var updated_at = ""
    @objc var is_admin = ""
    
    

    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            self.setValue(value, forKey: key)
        }
    }
    
    override init() {
        super.init()
    }
    
}

class CategoryDashboardData: NSObject {
    
    @objc var id = ""
    @objc var name = ""
    @objc var slug = ""
    @objc var status = ""
    @objc var icon = ""
    @objc var created_at = ""
    @objc var updated_at = ""
    
    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            self.setValue(value, forKey: key)
        }
    }
    
    override init() {
        super.init()
    }
    
}


class CommentsBaseData: NSObject {
    
    @objc var id = ""
    @objc var user_id = ""
    @objc var category_id = ""
    @objc var video = ""
    @objc var thumbnail = ""
    @objc var caption = ""
    @objc var donation_amount = ""
    @objc var is_prime = ""
    @objc var status = ""
    @objc var created_at = ""
    @objc var updated_at = ""
    @objc var views_count = ""
    @objc var comments_count = ""
    @objc var shares_count = ""
    @objc var wishlist_count = ""
    @objc var donation_received = ""
    @objc var user = [UserDashboardData]()
    @objc var category = [CategoryDashboardData]()
    @objc var comment = [CommentData]()
    

    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            if let v = value as? Dictionary<String,Any> {
                
                if key == "user" {
                        var arr = [UserDashboardData]()
                        let modal = UserDashboardData(data: v )
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }else if key == "category" {
                        var arr = [CategoryDashboardData]()
                        let modal = CategoryDashboardData(data: v)
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }
                
            }else if let v = value as? Array<Dictionary<String,Any>>{
                if key == "comment"{
                    var arr = [CommentData]()
                    for dataModal in v {
                        let modal = CommentData(data: dataModal)
                        arr.append(modal)
                    }
                    self.setValue(arr, forKey: key)
                }
            }else{
                self.setValue(value, forKey: key)
            }
            
        }
    }
    
    override init() {
        super.init()
    }
    
}


class CommentData: NSObject {
    
    @objc var id = ""
    @objc var user_id = ""
    @objc var donation_request_id = ""
    @objc var parent_id = ""
    @objc var tag_id = ""
    @objc var comment_type = ""
    @objc var comment = ""
    @objc var created_at = ""
    @objc var updated_at = ""
    @objc var comment_user = [UserDashboardData]()
    @objc var comment_tag = [UserDashboardData]()
    @objc var replies = [RepliesData]()

    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            if let v = value as? Dictionary<String,Any> {
                
                if key == "comment_user" {
                        var arr = [UserDashboardData]()
                        let modal = UserDashboardData(data: v )
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }else if key == "comment_tag" {
                        var arr = [UserDashboardData]()
                        let modal = UserDashboardData(data: v)
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }
                
            }else if let v = value as? Array<Dictionary<String,Any>>{
                if key == "replies"{
                    var arr = [RepliesData]()
                    for dataModal in v {
                        let modal = RepliesData(data: dataModal)
                        arr.append(modal)
                    }
                    self.setValue(arr, forKey: key)
                }
            }else{
                self.setValue(value, forKey: key)
            }
            
        }
    }
    
    override init() {
        super.init()
    }
    
}

class RepliesData: NSObject {
    
    @objc var id = ""
    @objc var user_id = ""
    @objc var donation_request_id = ""
    @objc var parent_id = ""
    @objc var tag_id = ""
    @objc var comment_type = ""
    @objc var comment = ""
    @objc var created_at = ""
    @objc var updated_at = ""
    @objc var replies_user = [UserDashboardData]()
    @objc var replies_tag = [UserDashboardData]()
    @objc var replies = [String]()
    

    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            if let v = value as? Dictionary<String,Any> {
                if key == "replies_user" {
                        var arr = [UserDashboardData]()
                        let modal = UserDashboardData(data: v )
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }else if key == "replies_tag" {
                        var arr = [UserDashboardData]()
                        let modal = UserDashboardData(data: v)
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }
            }else if let v = value as? Array<Dictionary<String,Any>> {
                print(v)
            }else{
                self.setValue(value, forKey: key)
            }
            
        }
    }
    
    override init() {
        super.init()
    }
    
}

class DonationCategoryData: NSObject {
    
    @objc var id = ""
    @objc var name = ""
    @objc var slug = ""
    @objc var icon = ""
    @objc var created_at = ""
    @objc var updated_at = ""

    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            self.setValue(value, forKey: key)
        }
    }
    
    override init() {
        super.init()
    }
    
}


class DonationBaseData: NSObject {
    
    @objc var id = ""
    @objc var donation_to = ""
    @objc var donation_by = ""
    @objc var donation_request_id = ""
    @objc var amount = ""
    @objc var status = ""
    @objc var created_at = ""
    @objc var updated_at = ""
    @objc var donation = [DonationData]()
    @objc var donation_request = [DonationRequestData]()
   
    

    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            if let v = value as? Dictionary<String,Any> {
                if key == "donation" {
                        var arr = [DonationData]()
                        let modal = DonationData(data: v )
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }else if key == "donation_request" {
                        var arr = [DonationRequestData]()
                        let modal = DonationRequestData(data: v)
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }
            }else if let v = value as? Array<Dictionary<String,Any>> {
                print(v)
            }else{
                self.setValue(value, forKey: key)
            }
            
        }
    }
    
    override init() {
        super.init()
    }
    
}

class DonationData: NSObject {
    
    @objc var id = ""
    @objc var name = ""
    @objc var status = ""
    @objc var email = ""
    @objc var country_code = ""
    @objc var phone = ""
    @objc var notification = ""
    @objc var image = ""
    @objc var username = ""
    @objc var university = ""
    @objc var occupation = ""
    @objc var address = ""
    @objc var dob = ""
    @objc var role = ""
    @objc var about = ""
    @objc var screen = ""
    @objc var created_at = ""
    @objc var updated_at = ""
    @objc var is_admin = ""
   
    

    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            
                self.setValue(value, forKey: key)
            
        }
    }
    
    override init() {
        super.init()
    }
    
}

class DonationRequestData: NSObject {
    
    @objc var id = ""
    @objc var user_id = ""
    @objc var category_id = ""
    @objc var video = ""
    @objc var thumbnail = ""
    @objc var caption = ""
    @objc var donation_amount = ""
    @objc var is_prime = ""
    @objc var status = ""
    @objc var created_at = ""
    @objc var updated_at = ""
    @objc var user = [UserDashboardData]()
    @objc var category = [CategoryDashboardData]()
  
   
    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            if let v = value as? Dictionary<String,Any> {
                
                if key == "user" {
                        var arr = [UserDashboardData]()
                        let modal = UserDashboardData(data: v )
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }else if key == "category" {
                        var arr = [CategoryDashboardData]()
                        let modal = CategoryDashboardData(data: v)
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }
                
            }else if let v = value as? Array<Dictionary<String,Any>>{
                if key == "comment"{
                    var arr = [CommentData]()
                    for dataModal in v {
                        let modal = CommentData(data: dataModal)
                        arr.append(modal)
                    }
                    self.setValue(arr, forKey: key)
                }
            }else{
                self.setValue(value, forKey: key)
            }
            
        }
    }
    
    override init() {
        super.init()
    }
    
}


class WishlistData: NSObject {
    
    @objc var id = ""
    @objc var selectedDonationAmount = ""
    @objc var user_id = ""
    @objc var category_id = ""
    @objc var video = ""
    @objc var thumbnail = ""
    @objc var caption = ""
    @objc var donation_amount = ""
    @objc var is_prime = ""
    @objc var status = ""
    @objc var created_at = ""
    @objc var updated_at = ""
    @objc var views_count = ""
    @objc var comments_count = ""
    @objc var shares_count = ""
    @objc var wishlist_count = ""
    @objc var donation_received = ""
    @objc var user = [UserDashboardData]()
    @objc var category = [CategoryDashboardData]()
    @objc var wishlist = [WishlistObjectData]()
    

    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            if let v = value as? Dictionary<String,Any> {
                
                if key == "user" {
                        var arr = [UserDashboardData]()
                        let modal = UserDashboardData(data: v )
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }else if key == "category" {
                        var arr = [CategoryDashboardData]()
                        let modal = CategoryDashboardData(data: v)
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }else if key == "comment"{
                    var arr = [WishlistObjectData]()
                    let modal = WishlistObjectData(data: v)
                    arr.append(modal)
                    self.setValue(arr, forKey: key)
                }
                
            }else{
                self.setValue(value, forKey: key)
            }
            
        }
    }
    
    override init() {
        super.init()
    }
    
}

class WishlistObjectData: NSObject {
    
    @objc var id = ""
    @objc var user_id = ""
    @objc var session_id = ""
    @objc var item_id = ""
    @objc var created_at = ""
    @objc var updated_at = ""
    

    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            self.setValue(value, forKey: key)
        }
    }
    
    override init() {
        super.init()
    }
    
}


class VideoDetailDonerData: NSObject {
    
    @objc var id = ""
    @objc var user_id = ""
    @objc var category_id = ""
    @objc var video = ""
    @objc var thumbnail = ""
    @objc var caption = ""
    @objc var donation_amount = ""
    @objc var is_prime = ""
    @objc var status = ""
    @objc var created_at = ""
    @objc var updated_at = ""
    @objc var views_count = ""
    @objc var comments_count = ""
    @objc var shares_count = ""
    @objc var wishlist_count = ""
    @objc var donation_received = ""
    @objc var user = [UserDashboardData]()
    @objc var category = [CategoryDashboardData]()
    @objc var donors = [VideoDonersBaseData]()
    

    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            if let v = value as? Dictionary<String,Any> {
                
                if key == "user" {
                        var arr = [UserDashboardData]()
                        let modal = UserDashboardData(data: v )
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }else if key == "category" {
                        var arr = [CategoryDashboardData]()
                        let modal = CategoryDashboardData(data: v)
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }
            }else if let v = value as? Array<Dictionary<String,Any>>{
                if key == "donors"{
                    var arr = [VideoDonersBaseData]()
                    for dataModal in v {
                        let modal = VideoDonersBaseData(data: dataModal)
                        arr.append(modal)
                    }
                    self.setValue(arr, forKey: key)
                }
           
            }else{
                self.setValue(value, forKey: key)
            }
            
        }
    }
    
    override init() {
        super.init()
    }
    
}


class VideoDonersBaseData: NSObject {
    
    @objc var id = ""
    @objc var donation_to = ""
    @objc var donation_by = [DonationData]()
    @objc var donation_request_id = ""
    @objc var amount = ""
    @objc var status = ""
    @objc var created_at = ""
    @objc var updated_at = ""
   

    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            if let v = value as? Dictionary<String,Any> {
                if key == "donation_by" {
                        var arr = [DonationData]()
                        let modal = DonationData(data: v )
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }
                
            }else if let v = value as? Array<Dictionary<String,Any>> {
                print(v)
            }else{
                self.setValue(value, forKey: key)
            }
            
        }
    }
    
    override init() {
        super.init()
    }
    
}


class LazorNewsData: NSObject {
    
    @objc var desc = ""
    @objc var id = ""
    @objc var news_category_id = ""
    @objc var type = ""
    @objc var video = ""
    @objc var thumbnail = ""
    @objc var imgae = ""
    @objc var title = ""
    @objc var slug = ""
    @objc var created_at = ""
    @objc var updated_at = ""
    @objc var category = [CategoryDashboardData]()

    
    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            if let v = value as? Dictionary<String,Any> {
                
                if key == "category" {
                        var arr = [CategoryDashboardData]()
                        let modal = CategoryDashboardData(data: v)
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }
            }else{
                self.setValue(value, forKey: key)
            }
            
        }
    }
    
    override init() {
        super.init()
    }
    
}

class NotificationsMainData: NSObject {
    
    @objc var id = ""
    @objc var user_id = ""
    @objc var notification_type = ""
    @objc var created_at = ""
    @objc var updated_at = ""
    @objc var notification = [NotificationdData]()

    
    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            if let v = value as? Dictionary<String,Any> {
                
                if key == "notification" {
                        var arr = [NotificationdData]()
                        let modal = NotificationdData(data: v)
                        arr.append(modal)
                        self.setValue(arr, forKey: key)
                }
            }else{
                self.setValue(value, forKey: key)
            }
            
        }
    }
    
    override init() {
        super.init()
    }
    
}

class NotificationdData: NSObject {
    
    @objc var title = ""
    @objc var body = ""
 
    init(data:Dictionary<String,Any>) {
        super.init()
        for (key,value) in data {
            self.setValue(value, forKey: key)
        }
    }
    
    override init() {
        super.init()
    }
    
}

