//
//  String.swift
//  Lazorapp
//
//  Created by SPDY on 04/03/1943 Saka.
//

import Foundation
import UIKit


//MARK:- UIColor

let colorGreen: UIColor = UIColor(red: 0.58, green: 0.75, blue: 0.12, alpha: 1.00)
let colorLightGray: UIColor = UIColor(red: 0.86, green: 0.86, blue: 0.86, alpha: 1.00)
let colorDarkGray: UIColor = UIColor(red: 0.66, green: 0.66, blue: 0.66, alpha: 1.00)



//MARK:- Api's URL
let URL_BASE : NSString! = "https://sample.jploftsolutions.in/lazor/api/"

let URL_LOGIN         : NSString? = URL_BASE.appending("signin") as NSString?
let URL_REGISTERAPI   : NSString? = URL_BASE.appending("signup") as NSString?
let URL_VerifyOTP     : NSString? = URL_BASE.appending("signup-otp-verification") as NSString?
let URL_CreateProfile : NSString? = URL_BASE.appending("profile/create") as NSString?
let URL_GetQuestions  : NSString? = URL_BASE.appending("security-questions") as NSString?
let URL_SubmitQuestions    : NSString? = URL_BASE.appending("user-security-question/create") as NSString?
let URL_BankDetails   : NSString? = URL_BASE.appending("bank/create") as NSString?
let URL_CardDetails   : NSString? = URL_BASE.appending("card/create") as NSString?
let URL_IAM           : NSString? = URL_BASE.appending("profile/iam") as NSString?
let URL_AboutProfile  : NSString? = URL_BASE.appending("profile/about") as NSString?
let URL_SplashVideo   : NSString? = URL_BASE.appending("settings") as NSString?
let URL_DashboardList : NSString? = URL_BASE.appending("donations?page=") as NSString?
let URL_CommentsList  : NSString? = URL_BASE.appending("donation-request") as NSString?
let URL_Comments      : NSString? = URL_BASE.appending("donation/comment") as NSString?
let URL_Profile       : NSString? = URL_BASE.appending("profile") as NSString?
let URL_DonationCategory : NSString? = URL_BASE.appending("donation/categories") as NSString?
let URL_CreateDonationRequest : NSString? = URL_BASE.appending("donation-request/create") as NSString?
let URL_MyDonation : NSString? = URL_BASE.appending("my-donation?user_id=") as NSString?
let URL_MyDoners : NSString? = URL_BASE.appending("my-donor?user_id=") as NSString?
let URL_UserVideos : NSString? = URL_BASE.appending("donation-requests?user_id=") as NSString?
let URL_PasswordReset   : NSString? = URL_BASE.appending("password/reset") as NSString?
let URL_VerifyOTPForgotPassword     : NSString? = URL_BASE.appending("password/otp-verification") as NSString?
let URL_UpdatePassword  : NSString? = URL_BASE.appending("password/update") as NSString?

let URL_AddWishlist  : NSString? = URL_BASE.appending("donation-wishlist/create") as NSString?
let URL_RemoveWishlist  : NSString? = URL_BASE.appending("donation-wishlist/remove") as NSString?
let URL_Wishlist  : NSString? = URL_BASE.appending("donation-wishlists") as NSString?

let URL_UserPersonalDetails  : NSString? = URL_BASE.appending("profile/detail") as NSString?
let URL_UserBankDetails  : NSString? = URL_BASE.appending("bank/detail") as NSString?
let URL_ProfileEdit  : NSString? = URL_BASE.appending("profile/edit") as NSString?
let URL_DonerVideoDetails  : NSString? = URL_BASE.appending("donation/") as NSString?

let URL_LazorNews  : NSString? = URL_BASE.appending("news") as NSString?
let URL_Notifications  : NSString? = URL_BASE.appending("notifications") as NSString?


//MARK:- Base Strings
let  kAlertTitle : String                = "Lazor"
let  kUserData   : String                = "userData"
let  kUserID     : String                = "userID"
let  kAccessToken   : String             = "Access Token"
let  kCameraIssue : String               = "Camara Issues"

let  ACCEPTABLE_MOBILE_NUMBER            = "0123456789"

//MARK:- Validations Message
let msgLogout : String                   = "Are you sure you want to log out?"

let msgCommon:String!                    = "This field is required"
let msgOTP:String!                       = "Please enter OTP"
let msgAboutProfile:String!              = "Please enter about yourself"

let msgEmail:String!                     = "Please enter valid email"
let msgName:String!                      = "Please enter name"
let msgUserName:String!                  = "Please enter username"
let msgUniversity:String!                = "Please enter university"
let msgOccupation:String!                = "Please enter occupation"
let msgAddress:String!                   = "Please enter City & State Location"
let msgDOB:String!                       = "Please select DOB"
let msgPassword:String!                  = "Please enter password"
let msgConfirmPassword:String!           = "Please enter confirm password"
let msgPasswordMismatch:String!          = "password mismatch"

let msgMobile:String!                    = "Mobile number must be 10 digits"

let msgBankName:String!                  = "Please enter bank name"
let msgRoutingNumber:String!             = "Please enter rounting number"
let msgAccountNumber:String!             = "Please enter account number"

let msgCheckBoxTermsConditions:String!   = "Please accept T&C"
let msgCheckBoxPrivacyPolicy:String!     = "Please accept privacy policy"

let msgCardNumber:String!                = "Please enter card number"
let msgValidCardNumber:String!           = "Card number must be 16 digits"
let msgExpDate:String!                   = "Please enter exp date"
let msgExpYear:String!                   = "Please enter expire year"
let msgCVV:String!                       = "Please enter cvv"


let msgVideo:String!                     = "Please upload a video"
let msgCategory:String!                  = "Please select a category"
let msgVideoCaption:String!              = "Please enter video caption"
let msgDonationAmount:String!            = "Please enter donation amount"
