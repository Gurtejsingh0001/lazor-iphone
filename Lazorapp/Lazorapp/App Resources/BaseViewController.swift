//
//  BaseViewController.swift
//  Lazorapp
//
//  Created by SPDY on 05/03/1943 Saka.
//

import UIKit
import SVProgressHUD

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func showErrorMessage(_ message: String) {
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()

        let alertController = UIAlertController(title: kAlertTitle, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: { _ in
            alertWindow.isHidden = true
        }))
        
        alertWindow.windowLevel = UIWindow.Level.alert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }

    
    //MARK:- UIButtons Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }


}

extension UIView{
    func addGrayViewBorder(){
        layer.cornerRadius = 10
        layer.borderColor = colorLightGray.cgColor
        layer.borderWidth = 1
    }
    
    func addGreenViewBorder(){
        layer.cornerRadius = 10
        layer.borderColor = colorGreen.cgColor
        layer.borderWidth = 1
    }

}

extension UIButton {
   func addGreenBorderButton() {
    layer.borderWidth = 1
    layer.borderColor = colorGreen.cgColor
    layer.cornerRadius = 21
   }
}

