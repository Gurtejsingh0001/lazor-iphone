//
//  MyOwnTableView.swift
//  BrainyWood
//
//  Created by JPLoft_2 on 05/07/21.
//

import Foundation
import UIKit


class MyOwnTableView: UITableView {
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return self.contentSize
    }
    
    override var contentSize: CGSize {
        didSet{
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
    }
}
