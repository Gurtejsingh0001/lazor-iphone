//
//  StaticClass.swift
//  Lazorapp
//
//  Created by SPDY on 28/06/21.
//

import Foundation


class staticClass {
    
    static let shared = staticClass()
    
    let themeColor = UIColor(named: "ThemeColor")?.cgColor ?? UIColor.clear.cgColor
    
    
    class func getStoryboard_PreLogin() -> UIStoryboard? {
        let str = "PreLogin"
        return UIStoryboard(name: str, bundle: nil)
    }
    
    
    class func getStoryboard_Main() -> UIStoryboard? {
        let str = "Main"
        //print("story board is:-\(UIStoryboard(name: str, bundle: nil))")
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_DonetionRequest() -> UIStoryboard? {
        let str = "DonetionRequest"
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_MySubscription() -> UIStoryboard? {
        let str = "MySubscription"
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_MyHoldingArea() -> UIStoryboard? {
        let str = "MyHoldingArea"
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_MyDonors() -> UIStoryboard? {
        let str = "MyDonors"
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_MyDonetions() -> UIStoryboard? {
        let str = "MyDonations"
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_Profile() -> UIStoryboard? {
        let str = "Profile"
        return UIStoryboard(name: str, bundle: nil)
    }
   
    class func getStoryboard_CashOut() -> UIStoryboard? {
        let str = "CashOut"
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_LazorNews() -> UIStoryboard? {
        let str = "LazorNews"
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_Notifications() -> UIStoryboard? {
        let str = "Notifications"
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_Invite() -> UIStoryboard? {
        let str = "Invite"
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_Settings() -> UIStoryboard? {
        let str = "Settings"
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_VideoSplash() -> UIStoryboard? {
        let str = "VideoSplash"
        return UIStoryboard(name: str, bundle: nil)
    }

   
}
