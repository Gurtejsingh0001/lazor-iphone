//
//  AppDelegate.swift
//  Lazorapp
//
//  Created by JPLoft_2 on 19/05/21.
//

import Foundation
import UIKit
import CoreData
import Alamofire

class HelperClass {
    
    typealias ASCompletionBlock = (_ result: NSDictionary, _ error: Error?, _ success: Bool) -> Void
     typealias ASCompletionBlock1 = (_ result: NSArray, _ error: Error?, _ success: Bool) -> Void
    
    class func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func showPopupAlertController(sender : Any, message : String, title : String) -> Void{
        
        let alert = UIAlertController(title: title as String,
                                      message: message as String,
                                      preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style:.default) {
              UIAlertAction in
              NSLog("OK Pressed")
//            UIControl().sendAction(#selector(NSXPCConnection.suspend),
//                                          to: UIApplication.shared, for: nil)
          }
       
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel, handler: nil)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        UIApplication.shared.keyWindow?.rootViewController!.present(alert, animated: true,completion: nil)
    
    }
        
    class func htmlToAttributedString(string : String,fontSize : CGFloat,fontName : String) -> NSAttributedString{
           var attribStr = NSMutableAttributedString()
           
           do {//, allowLossyConversion: true
           
            attribStr = try NSMutableAttributedString(data: string.data(using: .utf8)!, options: [.documentType: NSMutableAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
               
               let textRangeForFont : NSRange = NSMakeRange(0, attribStr.length)
            attribStr.addAttributes([NSAttributedString.Key.font : UIFont(name: fontName, size:fontSize)!], range: textRangeForFont)
               
           } catch {
               print(error)
           }
           
           return attribStr
    }
    
    

    class func showActivity(on view: UIView, with color : UIColor) {
        DispatchQueue.main.async(execute: {() -> Void in
            let indicator = UIActivityIndicatorView(style: .gray)
        let halfButtonHeight: CGFloat = view.bounds.size.height / 2
        let buttonWidth: CGFloat = view.bounds.size.width
        indicator.center = CGPoint(x: buttonWidth - buttonWidth / 2, y: halfButtonHeight)
        indicator.color = color
        view.addSubview(indicator)
        indicator.startAnimating()
        })
    }
    
    class func hideActivity(fromView view: UIView) {
        DispatchQueue.main.async(execute: {() -> Void in
        
            let subviewsEnum :NSEnumerator = (view.subviews as NSArray).reverseObjectEnumerator()
            _ = UIView();
            for subview in subviewsEnum{
                if (subview is UIActivityIndicatorView) {
                    (subview as? UIActivityIndicatorView)?.stopAnimating()
                    (subview as AnyObject).removeFromSuperview()
                }
            }
            
        })
    }
    
        
    
    //MARK: - Service Methods
    
    
    class func requestForAllApiyWithBody(serverUrl urlString : String, completionHandler : @escaping ASCompletionBlock) -> Void {
        
        print(urlString)
        
        let accessToken = UserDefaults.standard.string(forKey: kAccessToken)
        let newAcess = accessToken ?? ""
        print(accessToken ?? "")
        
        let newToken = "Bearer " + String(describing: newAcess)
        print(newToken)
        
        let methodTypes: HTTPMethod = .get
        let headers : HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
            "X-Requested-With": "XMLHttpRequest",
            "cache-control": "no-cache",
            "User-Agent": "PostmanRuntime/7.28.1",
            "Accept-Encoding": "gzip, deflate, br",
            "Connection": "keep-alive",
            "Authorization": newToken
                
//                "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOTE4YWZlOTM0ZTJjMDIxOTEyZmU3ZWJhMGE0MWQ0YjkxZmI3ZDU2MjNiZTVkOWI5YWI3OTc2NjY4ODRmMTdkODc5NDJmMGQ1YTc1NDM4MGQiLCJpYXQiOjE2MjM4MzA1NjAuMzgxMjgsIm5iZiI6MTYyMzgzMDU2MC4zODEzMSwiZXhwIjoxNjU1MzY2NTYwLjMwNDIxNywic3ViIjoiMSIsInNjb3BlcyI6W119.Sc0n9xy3jzNztX4Uqle5KR936WawAz2A9H4F8r3CuEXDXgRG8Yg7uh6xlig-n1cxKkoIcm5fqCQ4Dz-V2qvDl9uMPQYS_kV14kVAg8vnkZ_JkZO0zsLG6wNMetuGltFrSsizKcAGOeOFU4BoIxV3RudhQhrh1Yl0YU_zMShAHr2B-R7NElozKBzJhy9PEtWV7dU8LpUy4lFuV6CK3OTJ8jjseUxf7osQgWUf8VlmYG3uGm3oghM2j1355SkEHKaybM_EIiiVYR1uFX2SNWt6D-yhCpIWFKN2zi57e4eeFDLYIM64PEGtZkv_osQaFI6alTdEFqv1j7jk4DFOuMBSV9CjAtjGOqwLJXB04bcdoJIZwOfrz-aMDvtIeyCRMbQtfSf6-VF7lJ6R2-SqXJVjAu_Tybph2A_QFO57N9jEmrW980Iw4noW5_TdVPx0vwZcvbXmDZQHHWY0uh2sCzOqUxHUDEA-OYT2B6l3ZfgJQnkLcqLHT7uOAHWjUu2x3-W-H0Vor34q-sftii_upAmBb_YWGWtemvLGpbh50D0DZnFp1IF7haOefRZVt705uEvtcdAOD3Nk68kk3I16mG_0Gv2COqU8JaOiPnvjMdprsN7JsdDmZrZHnTA40B6urwGBdDbzyUrIJ9FiUxqsLCzCUP6FmneIOTBUxWK7NG3bfl8",
            
        ]
        
        
        AF.request(urlString, method: methodTypes,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
            switch response.result {
            case .success:
                print(response)
                if response.data != nil {
                    do {
                        let json = try JSONSerialization.jsonObject(with: response.data!, options: [])
                        let results = try? JSONSerialization.jsonObject(with: response.data!, options: [])
                        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: results! , options: .prettyPrinted)
                        let myString = String(data: jsonData!, encoding: String.Encoding.utf8)
                        print("Result: \(myString ?? "")")
                        
                        completionHandler(json  as! Dictionary<String, Any> as NSDictionary, nil, true)
                        return
                    }catch {
                        print(error.localizedDescription)
                        completionHandler([:], error, false)
                        return;
                    }
                }
                break
            case .failure(let error):
                print(error)
                completionHandler([:], error, false)
                return;
            }
        }
    }

    /*request.setValue(">t~E/x^sB9m>7CG1==k}M3N4iSmB-UY1~Om6rE{yiMwk1r=tVv}]MPdMR4Du?^77", forHTTPHeaderField: "authKey")
     request.setValue("ciVxvtAsfWU:APA91bFd", forHTTPHeaderField: "authenticationKey")*/
    
    class func requestForAllApiyWithBody( param : [String : Any],serverUrl urlString : String,andHeader isHeader : Bool , completionHandler : @escaping ASCompletionBlock) -> Void{
        
        //  var strSplglobal = String()
        print(urlString)
 //       let splalgoval = UserDefaults.standard.value(forKey: kSpecglobalKey) != nil ? UserDefaults.standard.value(forKey: kSpecglobalKey) : ""
        //        if isHeader && UserDefaults.standard.value(forKey: kUserData) != nil {
        //            let data = UserDefaults.standard.value(forKey: kUserData) as! Dictionary<String,Any>
        //            strSplglobal = data["specailvaltimeignore"] as! String
        //            print(strSplglobal)
        //
        //        }
        let accessToken = UserDefaults.standard.string(forKey: kAccessToken)
        let newAcess = accessToken ?? ""
        print(accessToken ?? "")
        print(newAcess)
        
        let newToken = "Bearer " + String(describing: newAcess)
        print(newToken)
        
        let headers : HTTPHeaders = [
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "cache-control": "no-cache",
            "Authorization": newToken
                
//                "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOTE4YWZlOTM0ZTJjMDIxOTEyZmU3ZWJhMGE0MWQ0YjkxZmI3ZDU2MjNiZTVkOWI5YWI3OTc2NjY4ODRmMTdkODc5NDJmMGQ1YTc1NDM4MGQiLCJpYXQiOjE2MjM4MzA1NjAuMzgxMjgsIm5iZiI6MTYyMzgzMDU2MC4zODEzMSwiZXhwIjoxNjU1MzY2NTYwLjMwNDIxNywic3ViIjoiMSIsInNjb3BlcyI6W119.Sc0n9xy3jzNztX4Uqle5KR936WawAz2A9H4F8r3CuEXDXgRG8Yg7uh6xlig-n1cxKkoIcm5fqCQ4Dz-V2qvDl9uMPQYS_kV14kVAg8vnkZ_JkZO0zsLG6wNMetuGltFrSsizKcAGOeOFU4BoIxV3RudhQhrh1Yl0YU_zMShAHr2B-R7NElozKBzJhy9PEtWV7dU8LpUy4lFuV6CK3OTJ8jjseUxf7osQgWUf8VlmYG3uGm3oghM2j1355SkEHKaybM_EIiiVYR1uFX2SNWt6D-yhCpIWFKN2zi57e4eeFDLYIM64PEGtZkv_osQaFI6alTdEFqv1j7jk4DFOuMBSV9CjAtjGOqwLJXB04bcdoJIZwOfrz-aMDvtIeyCRMbQtfSf6-VF7lJ6R2-SqXJVjAu_Tybph2A_QFO57N9jEmrW980Iw4noW5_TdVPx0vwZcvbXmDZQHHWY0uh2sCzOqUxHUDEA-OYT2B6l3ZfgJQnkLcqLHT7uOAHWjUu2x3-W-H0Vor34q-sftii_upAmBb_YWGWtemvLGpbh50D0DZnFp1IF7haOefRZVt705uEvtcdAOD3Nk68kk3I16mG_0Gv2COqU8JaOiPnvjMdprsN7JsdDmZrZHnTA40B6urwGBdDbzyUrIJ9FiUxqsLCzCUP6FmneIOTBUxWK7NG3bfl8",
            //     "splalgoval": splalgoval,
            //      "authKey":"k&$ko[*qp%rWUD< Kcxr|e$0,9Zni&7v7R}-ZCq|A_T{mcX^EWRd_N;tXPk; @nX",
            //      "authenticationKey":"ciVxvtAsfWU:APA91bFd"
        ]
        
        let methodTypes: HTTPMethod = .post
        
        AF.request(urlString, method: methodTypes, parameters:param ,encoding: JSONEncoding.default, headers: headers as? HTTPHeaders).responseJSON {
            response in
            switch response.result {
            case .success:
                print(response)
                if response.data != nil {
                    do {
                        let json = try JSONSerialization.jsonObject(with: response.data!, options: [])
                        let results = try? JSONSerialization.jsonObject(with: response.data!, options: [])
                        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: results! , options: .prettyPrinted)
                        let myString = String(data: jsonData!, encoding: String.Encoding.utf8)
                        print("Result: \(myString ?? "")")
                        
                        completionHandler(json  as! Dictionary<String, Any> as NSDictionary, nil, true)
                        return
                    }catch {
                        print(error.localizedDescription)
                        completionHandler([:], error, false)
                        return;
                    }
                }
                break
            case .failure(let error):
                print(error)
                completionHandler([:], error, false)
                return;
            }
        }
    }
    
    class func formRequestApiWithBody( param : NSDictionary, urlString : NSString, imgData : Data?, isHeader : Bool , completionHandler : @escaping ASCompletionBlock) -> Void {
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        
        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: param, options:.prettyPrinted)
        let myString = String(data: jsonData!, encoding: String.Encoding.utf8)
        
        print("Request URL: \(urlString)")
        print("Data: \(String(describing: myString))")
        
        var request = URLRequest(url: URL(string: urlString as String)!, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 45)
        
        request.httpMethod = "POST"
        
//        if isHeader {
//            request.setValue(UserDefaults.standard.value(forKey: "") as? String, forHTTPHeaderField: "splalgoval")
//        }
//        if UserDefaults.standard.string(forKey: kSpecglobalKey) != ""{
//            let value = UserDefaults.standard.string(forKey: kSpecglobalKey)
//            request.setValue(value, forHTTPHeaderField: "splalgoval")
//        }
        
        var body = Data()
        let boundary: String = "0xKhTmLbOuNdArY"
        let kNewLine: String = "\r\n"
        let contentType: String = "multipart/form-data; boundary=\(boundary)"
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        // Add the parameters from the dictionary to the request body
        let arrKeys = param.allKeys as! Array<String>;
        for name: String in arrKeys {
            let value: Data? = "\( param[name] as! String)".data(using: String.Encoding.utf8)
            body.append("--\(boundary)\(kNewLine)".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(name)\"".data(using: String.Encoding.utf8)!)
            // For simple data types, such as text or numbers, there's no need to set the content type
            body.append("\(kNewLine)\(kNewLine)".data(using: String.Encoding.utf8)!)
            body.append(value!)
            body.append(kNewLine.data(using: String.Encoding.utf8)!)
        }
        
        // Add the image to the request body
        if imgData != nil {
            body.append("--\(boundary)\(kNewLine)".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"uploadPhoto\"; filename=\"uploadPhoto.jpeg\"\("image")".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: image/jpeg".data(using: String.Encoding.utf8)!)
            body.append("\(kNewLine)\(kNewLine)".data(using: String.Encoding.utf8)!)
            body.append(imgData ?? Data())
            print(imgData ?? Data())
            body.append(kNewLine.data(using: String.Encoding.utf8)!)
        }
        // Add the terminating boundary marker to signal that we're at the end of the request body
        body.append("--\(boundary)--".data(using: String.Encoding.utf8)!)
        
        request.httpBody = body;
        var postDataTask = URLSessionDataTask()
        postDataTask.priority = URLSessionTask.highPriority
        
        postDataTask = session.dataTask(with: request, completionHandler: { (data : Data?,response : URLResponse?, error : Error?) in
            //            var json : (Any);
            if data != nil && response != nil {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: [])
                    let results = try? JSONSerialization.jsonObject(with: data!, options: [])
                    let jsonData: Data? = try? JSONSerialization.data(withJSONObject: results! , options: .prettyPrinted)
                    let myString = String(data: jsonData!, encoding: String.Encoding.utf8)
                    print("Result: \(myString ?? "")")
                    
                    completionHandler(json as! NSDictionary, nil, true)
                    return
                }catch {
                    print(error.localizedDescription)
                    completionHandler([:], error, false)
                    return;
                }
                
            }else if error != nil {
                completionHandler([:], error, false)
            }else {
                completionHandler([:], nil, false)
            }
        })
        postDataTask.resume()
        
    }
    
    class func addShadowToView(view:UIView, radius:CGFloat, height:CGFloat) -> Void {
        view.layer.masksToBounds = false
        view.layer.borderColor = UIColor .groupTableViewBackground.cgColor
        view.layer.shadowColor = UIColor .darkGray.cgColor
        view.layer.shadowOpacity = 0.8
        view.layer.shadowRadius = radius
    
        view.layer.shadowOffset = CGSize.init(width: 0.0, height: height)
    }
    
    class func addShadowToView(view:UIView, radius:CGFloat) -> Void {
        view.layer.masksToBounds = false
        //view.layer.borderColor = UIColor .groupTableViewBackground.cgColor
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOpacity = 0.8
        view.layer.shadowRadius = radius
        
        view.layer.shadowOffset = CGSize.init(width: 0.0, height: 0.0)
    }
    
    class func validateEmail(EmailAddress: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: EmailAddress)
    }
    
    class func getLength(mobileNumber: String) -> Int {
        
        var strMobile =  mobileNumber;
        strMobile = strMobile.replacingOccurrences(of: "(", with: "")
        strMobile = strMobile.replacingOccurrences(of: ")", with: "")
        strMobile = strMobile.replacingOccurrences(of: " ", with: "")
        strMobile = strMobile.replacingOccurrences(of: "-", with: "")
        strMobile = strMobile.replacingOccurrences(of: "+", with: "")
        
        //Calculate lenght of string
        let length = strMobile.count;
        
        return length
    }
    
   class func RBResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize.init(width: size.width * heightRatio, height: size.height * heightRatio)//CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSize.init(width: size.width * widthRatio, height: size.height * widthRatio)//CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect.init(x: 0, y: 0, width: newSize.width, height: newSize.height)//CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }


    
}

