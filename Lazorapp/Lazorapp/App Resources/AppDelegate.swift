//
//  AppDelegate.swift
//  Lazorapp
//
//  Created by JPLoft_2 on 19/05/21.
//

import UIKit
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
  //  var loadedEnoughToDeepLink : Bool = false
  //  var deepLink : RemoteNotificationDeepLink?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        Thread.sleep(forTimeInterval: 1.0)
        // Override point for customization after application launch.
        if #available(iOS 13.0, *) {
                    window?.overrideUserInterfaceStyle = .light
                }
      
        
        let data = UserDefaults.standard.dictionary(forKey: kUserData)
        print(data as Any)
        if data != nil{
            let strMain = UIStoryboard(name: "Main", bundle: nil)
            let vc = strMain.instantiateViewController(withIdentifier: "DashboardNavigation")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = vc
        }
       
        IQKeyboardManager.shared.enable = true
        return true
    }

//    internal func application(_ app: UIApplication, open url: URL,
//                     options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        if let scheme = url.scheme,
//            scheme.localizedCaseInsensitiveCompare("com.myApp") == .orderedSame,
//            let view = url.host {
//
//            var parameters: [String: String] = [:]
//            URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
//                parameters[$0.name] = $0.value
//            }
//
//            redirect(to: view, with: parameters)
//
//        }
//        return true
//    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {

        // TODO: handle URL from here

        return true
    }
}

//    private func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
//
//        if url.host == nil
//        {
//            return true;
//        }
//
//        let urlString = url.absoluteString
//        let queryArray = urlString!.componentsSeparatedByString("/")
//        let query = queryArray[2]
//
//        // Check if article
//        if query.rangeOfString("article") != nil
//        {
//            let data = urlString!.componentsSeparatedByString("/")
//            if data.count >= 3
//            {
//                let parameter = data[3]
//                let userInfo = [RemoteNotificationDeepLinkAppSectionKey : parameter ]
//                self.applicationHandleRemoteNotification(application, didReceiveRemoteNotification: userInfo)
//            }
//        }
//
//        return true
//    }
//
//    func applicationHandleRemoteNotification(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject])
//    {
//        if application.applicationState == UIApplicationState.Background || application.applicationState == UIApplicationState.Inactive
//        {
//            var canDoNow = loadedEnoughToDeepLink
//
//            self.deepLink = RemoteNotificationDeepLink.create(userInfo)
//
//            if canDoNow
//            {
//                self.triggerDeepLinkIfPresent()
//            }
//        }
//    }
//
//    func triggerDeepLinkIfPresent() -> Bool
//    {
//        self.loadedEnoughToDeepLink = true
//        var ret = (self.deepLink?.trigger() != nil)
//        self.deepLink = nil
//        return ret
//    }
    


